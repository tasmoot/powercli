$xvm = Get-VM  pythonserver

Get-VM $xvm | Select-Object Name,
    @{N="Tools Installed";E={$_.Guest.ToolsVersion -ne ""}},
    @{N="Tools Status";E={$_.ExtensionData.Guest.ToolsStatus}},
    @{N="Tools version";E={if($_.Guest.ToolsVersion -ne ""){$_.Guest.ToolsVersion}}}