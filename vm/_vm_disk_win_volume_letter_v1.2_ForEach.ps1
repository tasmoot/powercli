﻿
$vms = (Get-Content C:\code\vmlist.txt)

$vm = Get-VM -Name $vms

# Run Get-VMGuestDisk against VMs, then run those drives against Get-HardDisk
# New-Object allows you to select properties from both Get-HardDisk and Get-VMGuestDisk to include in the results.

# Set initial variables

$result = @() 

# Run Get-VMGuestDisk against VMs, then run those drives against Get-HardDisk
# New-Object allows you to select properties from both Get-HardDisk and Get-VMGuestDisk to include in the results.


$result = Get-VMGuestDisk -VM $vm | Foreach-Object{

$guestDisk = $_
$hardDisk = Get-HardDisk -VMGuestDisk $guestDisk

[PSCustomObject] @{
    VMName = $hardDisk.Parent
    #VMGuest = $guestDisk.VMGuest
    #DiskID = $hardDisk.ID
    #VMDK = $hardDisk.Filename
    DiskName = $hardDisk.Name
    DriveLetter = $guestDisk.DiskPath
    CapacityGB = [math]::Round($guestDisk.CapacityGB,2)
    FreeSpaceGB = [math]::Round($guestDisk.FreeSpaceGB,2)
    }
}
 


#$result 
# Export result to CSV
$result | Export-Csv -LiteralPath 'C:\code\export\servers_diskletters.csv' –NoTypeInformation
#System.Management.Automation.PSCustomObject