$(foreach ($cluster in get-cluster) { get-view -ViewType VirtualMachine -Filter @{'name'='smootta1-rhel'} -SearchRoot $cluster.id |
 % { $vVM=$_; $_.Summary.CustomValue | 
    select @{N="VM Name";E={$vVM.Name}},@{N="Cluster";E={$cluster.name}},Key,Value,@{N="Key Name";E={$vKey=$_.Key;($vVM.AvailableField|?{$_.Key -eq $vKey}).Name}} }})|
     export-csv customfields2.csv