﻿$TimeStamp = Get-Date -Format “yyyy-MM-dd-hhmm”

$vmlist = (Get-Content C:\code\vmlist.txt) -join '|'

$vms = Get-View -ViewType VirtualMachine -Filter @{"Name"="$vmlist"} -Property `
Name,runtime.powerState,Guest.hostname,Guest.net,Config.Hardware.numCPU,Summary.Storage.Committed,
Summary.Storage.UnCommitted,Config.Hardware.MemoryMB,Runtime.Host,Guest.GuestFullName,Parent,
ResourcePool,Config.Hardware.Device,Config.version,guest.toolsversionstatus

$vmstorageinfo = foreach($vm in $vms)
{
   # $t = Get-View $vm.ResourcePool -Property Name,Parent
   # $datacenter = $t.Name
   
    $vm.Config.Hardware.Device | where {$_.GetType().Name -eq "VirtualDisk"} |
    Select  @{N="VMName";E={$vm.Name}},
 
    @{N='IP Address';E={[string]::Join(',',($vm.Guest.Net | %{$_.IpAddress | where{$_.Split('.').Count -eq 4} | %{$_}}))}},
    @{N='OS';E={$vm.Guest.GuestFullName}},
    @{N='Host';E={$script:esx = Get-View -Id $vm.Runtime.Host; $script:esx.name}},
    @{N='Status';E={$vm.runtime.powerState}},
    @{N="Disk capacity GB";E={$_.CapacityInKB| %{[math]::Round($_/1MB,2)}}}, 
    @{N="Disk datastore";E={$_.Backing.Filename.Split(']')[0].TrimStart('[')}},
    @{N="ThinProvisioned";E={$_.Backing.ThinProvisioned}},
    @{N="Disk Provision GB";E={$vm.Summary.Storage.UnCommitted| %{[math]::Round($_/1GB,2)}}},
    @{N="Disk Used GB";E={$vm.Summary.Storage.Committed| %{[math]::Round($_/1GB,2)}}},
    @{N="HW Version";E={$vm.Config.version}},
    @{N="Tools Status";E={$vm.guest.toolsversionstatus}}
}

$vmstorageinfo | Export-Csv C:\code\export\vm_storage_info.csv -NoTypeInformation

$vmstorageinfo
