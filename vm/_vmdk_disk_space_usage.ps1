﻿$report = @()
foreach($vm in Get-VM){

   Get-HardDisk -VM $vm | ForEach-Object {

   $HardDisk = $_

   $row = "" | Select VM, GuestName, Hostname, Cluster, Datastore, VMXpath, HardDisk, DiskType, CapacityGB, DiskFreespace, TotalVMFSConsumed, ProvisionType

   $row.VM = $VM.Name

   $row.GuestName = $vm.Guest.HostName
   
   $row.Hostname = $vm.VMHost.Name

   $row.Cluster = (Get-Cluster -VM $vm ).Name

   $row.Datastore = $HardDisk.Filename.Split("]")[0].TrimStart("[")

   $row.VMXpath = $HardDisk.FileName

   $row.HardDisk = $HardDisk.Name

   $row.CapacityGB = ("{0:f1}" -f ($HardDisk.CapacityKB/1MB))

   $row.DiskFreespace = $vm.Guest.Disks | Measure-Object FreeSpaceGB -Sum | Select -ExpandProperty Sum

   $row.DiskType = $HardDisk.get_DiskType()

   $row.TotalVMFSConsumed = $vm.get_UsedSpaceGB()

   $row.ProvisionType = $HardDisk.StorageFormat

   $report += $row

   }

}


$report | Export-csv -Path "c:\code\export\VM_Disk_UsageReport-$((Get-Date).ToString('yyyy-MM-dd-mm')).csv" -NoTypeInformation