$TimeStamp = Get-Date -Format “yyyy-MM-dd-hhmm”
$report = @()

foreach ($dc in Get-Datacenter) {

    foreach ($cluster in Get-Cluster -Location $dc){

      $vms = Get-view -ViewType VirtualMachine -SearchRoot $cluster.ExtensionData.MoRef

      foreach ($vm in $vms){

        $info = "" | select Datacenter,Cluster,VmHost,Folder,Name,ToolsStatus,NumCpu,MemoryMB,GuestOs,Hostname,IPAddress,dvPortGroup,Datastore,DatastoreUsedGB
        $info.IPAddress = ($vm.Guest.net.IPAddress | where{$_} | Sort-Object -Unique) -join '|'
        $info.dvPortGroup = (Get-View -Id $vm.Network -Property Name).Name -join '|'
        $info.Datastore = (Get-View -Id $vm.Datastore -Property Name).Name -join '|'
        $info.DatastoreUsedGB = [math]::Round(($vm.Storage.PerDatastoreUsage.Committed | Measure-Object -Sum).Sum/1GB,1)
        $info.datacenter = $dc.name
        $info.Name = $vm.name
        $info.Folder = (Get-View -Id $vm.Parent -Property Name).Name -join '|'
        $info.Hostname = $vm.Guest.HostName
        $info.toolsstatus = $vm.guest.toolsstatus
        $info.NumCpu = $vm.Summary.config.NumCpu
        $info.MemoryMB = $vm.Summary.config.memorySizeMB
        $info.guestos = $vm.guest.guestfullname
        $info.VmHost = (Get-View -Id $vm.Runtime.Host -Property Name).Name -join '|'
        $info.Cluster = $cluster
        $report += $info

      }

    }

}

$report #| export-csv -Path /Users/smootta1/exports/VM_Report-$global:DefaultVIServer-$TimeStamp.csv -NoTypeInformation