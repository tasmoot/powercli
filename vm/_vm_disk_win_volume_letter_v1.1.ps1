﻿# Set initial variables
$resultFilePath = 'C:\code\export\servers_diskletters.csv'
$vm = Get-VM -Name <MACHINE_NAME>-RSQL-DCON02

# Run Get-VMGuestDisk against VMs, then run those drives against Get-HardDisk
# New-Object allows you to select properties from both Get-HardDisk and Get-VMGuestDisk to include in the results.

$result = Get-VMGuestDisk -VM $vm | Foreach-Object{

$guestDisk = $_
$hardDisk = Get-HardDisk -VMGuestDisk $guestDisk

[PSCustomObject] @{
    VMName = $hardDisk.Parent
    #VMGuest = $guestDisk.VMGuest
    #DiskID = $hardDisk.ID
    VMDK = $hardDisk.Filename
    DiskName = $hardDisk.Name
    DriveLetter = $guestDisk.DiskPath
    CapacityGB = [math]::Round($guestDisk.CapacityGB,2)
    FreeSpaceGB = [math]::Round($guestDisk.FreeSpaceGB,2)
    }
} 

#$result 
# Export result to CSV
$result | Export-CSV -Path $resultFilePath –NoTypeInformation
#System.Management.Automation.PSCustomObject