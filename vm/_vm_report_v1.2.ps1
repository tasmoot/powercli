$report = @()
#$CustomAttributeOrg = Get-CustomAttribute -Name "Organization"
#$CustomAttributeEmail = Get-CustomAttribute -Name "Email"
foreach ($dc in Get-Datacenter) {

    foreach ($cluster in Get-Cluster -Location $dc){

      $vms = Get-view -ViewType VirtualMachine -SearchRoot $cluster.ExtensionData.MoRef
      #$vms = Get-view -ViewType VirtualMachine -Filter @{'name'='<MACHINE_NAME>-4CN-LAB01'} -SearchRoot $cluster.ExtensionData.MoRef
      #$vms = Get-view -ViewType VirtualMachine -Filter @{'name'='<MACHINE_NAME>-4CN-RLDEV06'} -SearchRoot $cluster.ExtensionData.MoRef
      
      foreach ($vm in $vms){

        $info = "" | Select-Object Name,vCenter,Datacenter,Cluster,VmHost,Folder,GuestOs,guestFQDN,NumCpu,`
        MemoryMB,IPAddress,dvPortGroup,NumVirtulDisks,Datastore,DiskAllocatedGB,DatastoreUsedGB,DiskAvailableGB,VMState,ToolsStatus,Notes,LastBackupStatus,LastSuccessfulBackup
        $info.Name = $vm.name
        $info.vCenter = ([System.Uri]$cluster.ExtensionData.Client.ServiceUrl).Host
        $info.datacenter = $dc.name
        $info.VmHost = (Get-View -Id $vm.Runtime.Host -Property Name).Name -join '|'
        $info.Folder = (Get-View -Id $vm.Parent -Property Name).Name -join '|'
        $info.guestos = $vm.guest.guestfullname
		    $info.guestFQDN = $vm.Guest.HostName
        $info.IPAddress = ($vm.Guest.net.IPAddress | Where-Object{$_} | Sort-Object -Unique) -join '|'
        $info.dvPortGroup = (Get-View -Id $vm.Network -Property Name).Name -join '|'
        #$info.dvPortGroup = (Get-View $vm.Network -Property Name).Name -join '|'
        $info.NumVirtulDisks = $vm.Summary.config.NumVirtualDisks
        $info.Datastore = (Get-View -Id $vm.Datastore -Property Name).Name -join '|'
        #$info.Datastore = (Get-View $vm.Datastore -Property Name).Name -join '|'
        $info.DiskAllocatedGB = [math]::Round(($vm.Guest.Disk.Capacity | Measure-Object -Sum).Sum/1GB,1)
        $info.DatastoreUsedGB = [math]::Round(($vm.Storage.PerDatastoreUsage.Committed | Measure-Object -Sum).Sum/1GB,1)
        $info.DiskAvailableGB = ($info.DiskAllocatedGB - $info.DatastoreUsedGB)
        $info.NumCpu = $vm.Summary.config.NumCpu
        $info.MemoryMB = $vm.Summary.config.memorySizeMB
        $info.VMState = $vm.guest.GuestState
		    $info.toolsstatus = $vm.guest.toolsstatus
		    $info.Notes = $vm.config.Annotation
        #$info.Backup = $vm.CustomFields.Item("LastBackupStatus-com.dellemc.avamar") | Select-Object -ExpandProperty Value
        #$info.LastBackup = ($vm | Get-Annotation -CustomAttribute "LastBackupStatus-com.dellemc.avamar" | Select-Object Value)
        #$info.LastBackupStatus = ($vm.CustomValue | Where-Object {$_.Key -eq "602"} | Select-Object -ExpandProperty Value)
        #$info.LastSuccessfulBackup = ($vm.CustomValue | Where-Object {$_.Key -eq "603"} | Select-Object -ExpandProperty Value)
                
        $info.Cluster = $cluster
        $report += $info

      }
     

    }

}

$report | export-csv -Path "c:\code\export\<MACHINE_NAME>_VM_Report_$((Get-Date).ToString('yyyy-MM-dd-mm')).csv" -NoTypeInformation
