﻿# Set initial variables
$resultFilePath = 'C:\code\export\servers_diskletters.csv'
$vm = Get-VM -Name <MACHINE_NAME>-4CN-ALS05

# Run Get-VMGuestDisk against VMs, then run those drives against Get-HardDisk
# New-Object allows you to select properties from both Get-HardDisk and Get-VMGuestDisk to include in the results.

$result = Get-VMGuestDisk -VM $vm | Foreach-Object{

$guestDisk = $_
$hardDisk = Get-HardDisk -VMGuestDisk $guestDisk

New-Object -TypeName PSObject -Property @{
    VMName = $hardDisk.Parent
    VMGuest = $guestDisk.VMGuest
    DriveLetter = $guestDisk.DiskPath
    CapacityGB = [math]::Round($guestDisk.CapacityGB)
    FreeSpaceGB = [math]::Round($guestDisk.FreeSpaceGB)
    DiskID = $hardDisk.ID
    DiskName = $hardDisk.Name
    VMDK = $hardDisk.Filename
    }
} 

$result

# Export result to CSV
# $result | Export-CSV -Path $resultFilePath -Force –NoTypeInformation