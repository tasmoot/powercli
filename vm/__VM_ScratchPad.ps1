get-datastore ntapcloud17_17 | get-vm -Name acc-a4 | Get-Snapshot | Select-Object VM,Created,Name,@{N='Size GB';E={{([Math]::round((SizeGB)),2)}}}

Get-Cluster B03Cluster100 | Get-View -ViewType VirtualMachine -Filter  | Select Name,@{n='Operating System'; e={$_.ExtensionData.Summary.Guest.GuestFullName}}, `

@{n='Hostname'; e={$_.ExtensionData.Summary.Guest.HostName}}, `
@{n='IP Address'; e={$_.ExtensionData.Summary.Guest.IpAddress}}, `
@{n='MacAddress'; e={$_.ExtensionData.Guest.Net.MacAddress}}, `
@{n='NumCPU'; e={$_.ExtensionData.summary.config.NumCpu}}, `
@{n='MemorySizeMB'; e={$_.ExtensionData.summary.config.MemorySizeMB}}, `
@{n='NIC Cards'; e={$_.ExtensionData.summary.config.NumEthernetCards}}, `
@{n='Virtual Disks'; e={$_.ExtensionData.summary.config.NumVirtualDisks}}, `
@{n='Template'; e={$_.ExtensionData.summary.config.Template}}, `
@{n='ToolsStatus'; e={$_.ExtensionData.Summary.Guest.ToolsStatus}}, `
@{n='Datastore Path'; e={$_.ExtensionData.Summary.Config.VmPathName}} `
Export-Csv -NoTypeInformation -Path ~/exports/VM_Report_Cluster100_2020-05-08.csv





function Get-xDetails {
    <#  
        .NOTES
        ===========================================================================
        Created by: Thomas Smoot
        ===========================================================================
        Changelog:  
        2020.02 ver 1.0 Base Release  
        ===========================================================================
        External Code Sources: 
        NONE

        .DESCRIPTION
        This Function returns extra details about VMs
    
        .Example
        Get-VM -Name MyVM | Get-VMDetails | Format-Table -AutoSize
    
        .PARAMETER myVMs
        VM(s) to process
    
    
    }
    #>
            param(
    
        [Parameter(Mandatory=$true,ValueFromPipeline=$true)]
    
        [VMware.VimAutomation.ViCore.Types.V1.Inventory.VirtualMachine[]]$Entity
    
        )
    
      process {
    $Entity | Select-Object Name,NumCPU,MemoryGB,PowerState,CustomFields,Notes,
    @{N='GuestOS';E={$_.Guest.OSFUllName}},
    @{N="GuestOsIpAddress"; E={(($_.Guest.IPaddress |  Where-Object {([IPAddress]$_).AddressFamily -eq [System.Net.Sockets.AddressFamily]::InterNetwork})) -join ", "}},
    @{N='PortgroupName';E={(Get-NetworkAdapter -VM $_).NetworkName -join '|'}}
    @{N='Email';E={(Get-Annotation -Entity $_)CustomAttribute}"Date").Value}
    
  }
}
