$CustomAttribute = Get-CustomAttribute -Name 'LastBackupStatus-com.dellemc.avamar'
[array]$VMs=@()
foreach ($cluster in get-cluster 'Main HA Cluster')  {
    foreach ($vmview in (Get-view -ViewType VirtualMachine -Filter @{'name'='<MACHINE_NAME>-4CN-RLDEV06'})) {
        $vm=New-Object PsObject
        Add-Member -InputObject $vm -MemberType NoteProperty -Name VMname -Value $vmview.Name
        Add-Member -InputObject $vm -MemberType NoteProperty -Name Cluster -Value $cluster.Name
        Add-Member -InputObject $vm -MemberType NoteProperty -Name $CustomAttribute.Name[0] -Value ($vmview.Summary.CustomValue | ? {$_.Key -eq $CustomAttribute.Key}).value
        $VMs+=$vm
    }
}
$VMs
