
Connect-VIServer $esxi -Credential $credential
 
Get-VM | New-AdvancedSetting -Name isolation.tools.copy.disable -Value false -Confirm:$false -Force:$true
      New-AdvancedSetting -Name isolation.tools.paste.disable -Value false -Confirm:$false -Force:$true
 
Disconnect-VIServer $esxi -Confirm:$false