
#$xvm = Get-View -ViewType VirtualMachine -Filter @{‘name’=’smootta1-win10’}
  

#$report = Get-View -ViewType VirtualMachine -Filter @{‘name’=’smootta1-win10’} | Select-Object Name, `
#-SearchRoot $cluster.ExtensionData.MoRef
$report = Get-View -ViewType VirtualMachine -Filter @{‘name’=’fmd-inx’} | Select-Object Name, `


@{n='Operating System'; e={$_.Summary.Guest.GuestFullName}}, `
@{n='Hostname'; e={$_.Summary.Guest.HostName}}, `
@{n='IP Address'; e={$_.Summary.Guest.IpAddress}}, `
@{n='MacAddress'; e={$_.Guest.Net.MacAddress}}, `
@{n='NumCPU'; e={$_.Summary.config.NumCpu}}, `
@{n='MemorySizeMB'; e={$_.Summary.config.MemorySizeMB}}, `
@{n='NIC Cards'; e={$_.Summary.config.NumEthernetCards}}, `
@{n='Virtual Disks'; e={$_.Summary.config.NumVirtualDisks}}, `
@{n='Capacity(GB)'; e={[math]::Round($_.Guest.Disk.Capacity/ 1GB)}}, `
@{n='Free Space(GB'; e={[math]::Round($_.Guest.Disk.FreeSpace / 1GB)}}, 
#@{N='Consumed Space';E={[math]::Round((($_.Guest.Disk.Capacity))- ($_.Guest.Disk.FreeSpace)),0)))} `
@{n='Template'; e={$_.Summary.config.Template}}, `
@{n='ToolsStatus'; e={$_.Summary.Guest.ToolsStatus}}, `
@{n='Datastore Path'; e={$_.Summary.Config.VmPathName}} `
@{n='VmHost'; e={$_.Runtime.Host}} 

#Export-Csv -NoTypeInformation -Path ~/exports/VM_Report_Cluster100_2020-05-08.csv

$report