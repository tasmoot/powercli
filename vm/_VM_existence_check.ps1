$TimeStamp = Get-Date -Format “yyyy-MM-dd-hhmm”

# NOTE:  VM list text file MUST have header row of "Name"
Import-Csv -Path /Users/smootta1/code/vmware/powercli_lab/vm/vmlist.txt -UseCulture -pipelinevariable row |
foreach-object -process {

    $obj = New-Object -TypeName PSObject -Property @{

        Name = $row.Name

        Exist = $false

        PowerState = 'na'

    }

    try{

        $vm = Get-VM -Name $row.Name -ErrorAction Stop

        $obj.Exist = $true

        $obj.PowerState = $vm.PowerState

    }

    catch{}

    $obj

} | Export-Csv -LiteralPath /Users/smootta1/exports/VM-existence_check-$TimeStamp.csv -NoTypeInformation

#END