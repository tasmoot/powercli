$TimeStamp = Get-Date -Format “yyyy-MM-dd-hhmm”

#$vmlist = (Get-Content /Users/smootta1/code/vmware/powercli_lab/vm/vmlist.txt) -join '|'

$vms = Get-View -ViewType VirtualMachine <#-Filter @{"Name"="smootta1"} #> -Property `
Name,Summary.Storage.Committed,Config.Hardware.Device

$vmstorageinfo = foreach($vm in $vms)
{
   # $t = Get-View $vm.ResourcePool -Property Name,Parent
   # $datacenter = $t.Name
   
    $vm.Config.Hardware.Device | where {$_.GetType().Name -eq "VirtualDisk"} |
    Select  @{N="VMName";E={$vm.Name}},
 
    @{N="Disk capacity GB";E={$_.CapacityInKB| %{[math]::Round($_/1MB,2)}}}, 
    @{N="Disk datastore";E={$_.Backing.Filename.Split(']')[0].TrimStart('[')}},
    @{N="ThinProvisioned";E={$_.Backing.ThinProvisioned}},
    @{N="Disk Used GB";E={$vm.Summary.Storage.Committed| %{[math]::Round($_/1GB,2)}}}
 }

$vmstorageinfo | Export-Csv -LiteralPath /Users/smootta1/exports/VM-disk_provision_type-$TimeStamp.csv -NoTypeInformation

#$vmstorageinfo

 
