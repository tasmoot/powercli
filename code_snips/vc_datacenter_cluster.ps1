Get-VM <MACHINE_NAME>-vdi-1005 |
Select Name,@{N="ESX";E={$_.VMHost.Name}},
  @{N="Cluster";E={Get-Cluster -VM $_ | Select -ExpandProperty Name}},
  @{N="Datacenter";E={Get-Datacenter -VM $_ | Select -ExpandProperty Name}},
  @{N="vCenter";E={$_.ExtensionData.Client.ServiceUrl.Split('/')[2]}}