function Get-VMDetails {
    <#  
        .NOTES
        ===========================================================================
        Created by: Thomas Smoot
        ===========================================================================
        Changelog:  
        2020.02 version 1.0 Base
        2022.09 version 1.1 Added Some
        ===========================================================================
        External Code Sources: 
        NONE

        .DESCRIPTION
        This Function returns extra details about VMs
    
        .Example
        Get-VM -Name MyVM | Get-VMDetails | Format-Table -AutoSize
    
        .PARAMETER myVMs
        VM(s) to process
    
    
    }
    #>
            param(
    
        [Parameter(Mandatory=$true,ValueFromPipeline=$true)]
    
        [VMware.VimAutomation.ViCore.Types.V1.Inventory.VirtualMachine[]]$Entity
    
        )
    
      process {
    $Entity | Select-Object Name,NumCPU,MemoryGB,PowerState,CustomFields,Notes,
    @{N='GuestOS';E={$_.Guest.OSFUllName}},
    @{N="GuestOsIpAddress"; E={(($_.Guest.IPaddress |  Where-Object {([IPAddress]$_).AddressFamily -eq [System.Net.Sockets.AddressFamily]::InterNetwork})) -join ", "}},
    @{N='PortgroupName';E={(Get-NetworkAdapter -VM $_).NetworkName -join '|'}},
    @{N='MacAddress';E={(Get-NetworkAdapter -VM $_).MacAddress}},
   # @{N='PortgroupVlanId';E={(Get-VDPortGroup -Name (Get-NetworkAdapter -VM $_).NetworkName -VMHost $_.VMHost).VlanId -join '|'}},
    @{N='Cluster';E={(Get-Cluster -VM $_).Name}},
    @{N='EsxHostName';E={$_.VMHost.Name}},
    @{N='EsxHostVersion';E={$_.VMHost.Version}},
    #@{N='BiosUuid'; E={$_.ExtensionData.Config.UUID}},
    @{N='ProvisionedStorageGB'; E={[math]::round($_.ProvisionedSpaceGB,2)}},
    @{N='UsedStorageGB'; E={[math]::round($_.UsedSpaceGB,2)}},
    @{N="Hardware_Version"; E={$_.ExtensionData.Guest.HwVersion} },
    @{N="OsVersion_ReportedByTools"; E={$_.Guest.OSFullName } },
    @{N='OsVersion_Configure'; E={$_.ExtensionData.Config.GuestFullName}},
    @{N='ToolsStatus'; E={$_.ExtensionData.Guest.ToolsStatus}},
    @{N='ToolsVersion'; E={$_.ExtensionData.Guest.ToolsVersion}},
    @{N='ToolsRunningStatus'; E={$_.ExtensionData.Guest.ToolsRunningStatus}}
    
  }
}