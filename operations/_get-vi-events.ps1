Get-VIEvent -Start (Get-Date).adddays(-2) | `
where {$_.gettype().Name -eq "VmCreatedEvent" -and $_.CreatedTime -lt (Get-Date).adddays(-2)} | `
select @{N="VMname"; E={$_.Vm.Name}},
@{N="CreatedTime"; E={$_.CreatedTime}},
@{N="Host"; E={$_.Host.Name}},
@{N="User"; E={$_.UserName}}