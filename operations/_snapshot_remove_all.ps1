$xvm = Read-Host -Prompt "Enter VM Name"

Get-VM -Name $xvm | Get-Snapshot | Sort-Object -Property Created | Select-Object -First 1 | Remove-Snapshot -RemoveChildren -RunAsync -Confirm:$false
