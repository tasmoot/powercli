# Script To remove all snapshots for VMs specified in the $vms list
# Tom Smoot 2022-10-01
$timestamp = Get-Date -Format "yyyy-mm-dd-hh:ss" #Get the current date/time and place entry into log that a new session has started

$logfile = ".\VM_snapshot_remove_log1.txt" #Script's log file location

$vms = Get-vm (Get-Content C:\code\powercli\operations\vmsnapshotlist1.txt) #Read the TXT File into a Get-VM variable
Add-Content $logfile "#####################################################"
Add-Content $logfile "$timestamp New Session Started"
$message = "Deleting Snapshots from vmsnapshotlist1.txt"
Write-Host $message
Add-Content $logfile  $message

foreach ($vm in $vms) #Remove snapshots for each VM in the CSV
{
#$vm = $vm.VM #Load the virtual machine object
$snapshotcount = Get-VM $vm | Get-Snapshot | Measure-Object #Get the number of snapshots for the VM
$snapshotcount = $snapshotcount.Count #This line makes it easier to insert the number of snapshots into the log file
$timestamp = Get-Date #Get the current date/time and place entry into log that the script is going to remove x number of shapshots for the VM
$message = "$timestamp Removing $snapshotcount Snapshot(s) for VM $vm"
Write-Host $message
Add-Content $logfile  $message

Get-VM $vm | Get-Snapshot | Remove-Snapshot -confirm:$false -Verbose | Out-File $logfile -Append #Removes the VM's snapshot(s) and writes any output to the log file

$timestamp = Get-Date #Get the current date/time and place entry into log that the script has finished removing the VM's snapshot(s)

Add-Content $logfile "$timestamp Snapshots removed for $vm"
Write-Host "Pausing before starting the next one..."
Start-Sleep 60
Write-Host "Preparing to delete the next snapshot..."
Start-Sleep 10
}