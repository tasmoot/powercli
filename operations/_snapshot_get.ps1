#Get-VM | Get-Snapshot |
#$Report = @()
$xdatastore = Read-Host "Enter Datastore Name"
get-datastore $xdatastore | get-vm | Get-Snapshot| Select-Object @{N='VM';E={$_.VM.Name}},

@{N='Created';E={$_.Created.ToString("dd/MM/yyyy")}},
@{N ='Age';E={((Get-Date)-$_.Created).Days}},
@{N='SizeGB';E={[math]::Round($_.SizeGB,1)}},
@{N='Description';E={$_.Description}}  | Sort-Object SizeGB -Descending | Out-GridView
#| Export-Csv -LiteralPath ~/exports/ntap_snapshots_2021-09-24.csv

#One line delete
#get-snapshot -vm shortboard -id VirtualMachineSnapshot-snapshot-5260235 | Remove-Snapshot
