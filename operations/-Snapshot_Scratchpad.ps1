Report on snapshots
Get-VM server1 | Get-Snapshot | Select VM,Name,Created,SizeGB,Description

Get-VM server1 | Get-Snapshot | Remove-Snapshot

This will prompt you to confirm snapshot removal for each snapshot on the VM.

Get-VM TestVM | Get-Snapshot | Remove-Snapshot -confirm:$false

<!--The above command will delete each snapshot one by one.  If we’re planning to delete all the snapshots, 
why not just select the last snapshot and delete all the children (note:  if you have multiple snapshot chains on the VM this won’t have the intended effect), similar to a Delete All:
--!>
Get-VM TestVM | Get-Snapshot | Select-Object -Last 1 | Remove-Snapshot -Confirm:$false -RemoveChildren

That covers the basics for removing a snapshot from a single VM, but often we’ll need to delete snapshots from a collection of VMs.

To remove all snapshots on all VMs from a specific VM Folder:

Get-Folder ExampleFolder | Get-VM | Get-Snapshot | Remove-Snapshot -confirm:$false -RemoveChildren

To remove all snapshots from all VMs older than Feb 1, 2017:

Get-VM | Get-Snapshot | Where-Object {$_.Created -lt (Get-Date 1/Feb/2017)} | Remove-Snapshot -Confirm:$false

