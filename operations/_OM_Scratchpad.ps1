Connect-VIServer 

Connect-OMServer -Server aplvopsm.jhuapl.edu -AuthSource ActiveDirectory


#Get OM Status for VM
Get-VM server1 | Get-OMResource

#Get OM status of datastore
Get-Datastore datatore1 | Get-OMResource

#List datastores with issues
Get-Datastore datatore1 | Get-OMResource | Where-Object { $_.Health -ne "Green" }

#Get Alerts for datastore
Get-Datastore datatore1 | Get-OMResource | Where-Object { $_.Health -ne "Green" } | Get-OMAlert | Format-Table -Autosize

#List active alerts
Get-OMAlert -Status Active | ft -Autosize

#List the CPU stats for a VM
Get-VM server1 | Get-OMResource | Get-OMStat -cpu

#List the VMs that have CPU usage above 30%
Get-VM server1 | Get-OMResource | Get-OMStat -cpu | Where-Object { $_.value -gt 30 } | Format-Table -Autosize