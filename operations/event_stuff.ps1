$days = 120
$eventnumber = 9999
$report = @()
$serviceInstance = get-view ServiceInstance
$eventMgr = Get-View eventManager
if($eventMgr.Client.Version -eq "Vim4" -and $eventnumber -gt 1000){
	Write-Host "Sorry, API 4.0 only allows a maximum event window of 1000 entries!" -foregroundcolor red
	Write-Host "Please set the variable `$eventnumber to 1000 or less" -foregroundcolor red
	exit
	}
	$efilter = New-Object VMware.Vim.EventFilterSpec
	$efilter.time = New-Object VMware.Vim.EventFilterSpecByTime
	$efilter.time.beginTime = (Get-Date).Adddays(- $days)
	$efilter.time.endtime = (Get-Date).AddDays(-30)
	$ecollectionImpl = Get-View ($eventMgr.CreateCollectorForEvents($efilter))
	$ecollection = $ecollectionImpl.ReadNextEvents($eventnumber)
	while($ecollection -ne $null){
		foreach($event in $ecollection){
			switch($event.GetType()){
				"VMware.Vim.VmCreatedEvent" {
					$row = New-Object PSObject
					$row | Add-Member -name VMname -value $event.Vm.Name -memberType NoteProperty
					$row | Add-Member -name CreatedTime -value $event.CreatedTime -memberType NoteProperty
					$row | Add-Member -name Host -value $event.Host.Name -memberType NoteProperty
					$row | Add-Member -name User -value $event.UserName -memberType NoteProperty
					$report += $row
					}
					}
					}
					$ecollection = $ecollectionImpl.ReadNextEvents($eventnumber)
					}
					$ecollectionImpl.DestroyCollector()
					$report
				}
			}
		}
	}`"
}
