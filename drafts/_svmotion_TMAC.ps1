
$dsSource = "C138911-std04"
$dsDest = "C138911_tier2_02_cppod01_clstr01_NWK01"
$h = "H200979"
$Host.UI.RawUI.WindowTitle = "$h - $dsSource > $dsDest"
#For all VMS on a datastore uncomment the next line
$vms = Get-VM -Datastore $dsSource

#For a single VM uncomment the next line and insert the vm name
#$vms = Get-VM "VM NAME"

foreach($_ in $vms)
{
#                $source = get-vm $_.Name | get-datastore
                write-host "#######################"
                Get-Date -format G
                write-host `t "Moving VM:" $_.Name
                Move-VM $_.Name -Datastore $dsdest -DiskStorageFormat thin > $null
                write-host `t "Completed:" $VMServer.Name
                Get-Date -format G
                write-host "#######################"
}


