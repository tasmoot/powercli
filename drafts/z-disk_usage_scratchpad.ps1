﻿
    #orginal code
ForEach ($VM in (Get-VM -Name $xvm | Get-View)){($VM.Guest.Disk |
Select `
@{N=“Name“;E={$VM.Name}},DiskPath, @{N=“Capacity(GB)“; E={[math]::Round($_.Capacity/ 1GB)}}, `
@{N=“Free Space(GB)“;E={[math]::Round($_.FreeSpace / 1GB)}}, `
@{N=“Free Space %“;E={[math]::Round(((100 * ($_.FreeSpace))/ ($_.Capacity)),0)}}) | Format-Table -auto}
