# # # # # # # # # # # # # # # # # # # #  # # # # # # # # # # # # #
# Created by Tom Smoot ITSD/ISS on 2020-02-03 for APL.
#
# This Script will create multiple VMs from a text file.
#  
# 1) Create a text file with one VM name per line (NO BLANK LINES!)
# 2) CAREFULLY Modify all $ variables to reflect appropriate targets.  -DRS Cluster in auto mode can be used.
# 3) For automation purposes the correct spec must be used.
# 4) Change location value!   
# 5) Test this script with one new VM in the text file first to verify desired result!
#
# # # # # # # # # # # # # # # # # # # #  # # # # # # # # # # # # #


#$xspec = Get-OSCustomizationSpec -name Windows-NoDomain 


$xvm = Get-Content -Path "U:\code\vmware\vm-list-lab.txt"



ForEach ($VM in $xvm) {

    $xdatastore = Get-Datastore -name CloudNFS
    $xtemplate = get-template -name ITSD-Windows2012R2-Internet-EFI-02T
    $xvmhost = get-vmhost -name aplvws03y.dom1.jhuapl.edu
    $xfolder = Get-Folder -Name VMware-Lab

    New-VM -Name $VM -Template $xtemplate -VMHost $xvmhost -datastore $xdatastore -DiskStorageFormat Thin -description "NTAP Test VMs" -location "$xfolder" -confirm:$false
    }


<##

ForEach ($VM in $xvm) {

    $xdatastore = Get-Datastore -name CloudNFS
    $xtemplate = get-template -name ITSD-Windows2012R2-Internet-EFI-02T
    $xvmhost = get-vmhost -name aplvws03y.dom1.jhuapl.edu
    $xfolder = Get-Folder -Name VMware-Lab

    $vTasks += New-VM -Name $VM -Template $xtemplate -VMHost $xvmhost -datastore $xdatastore -DiskStorageFormat Thin -description "NTAP Test VMs" -location "$xfolder" -confirm:$false
    }

##>