# This is a PowerCLI script that I use to Storage vMotion (s/vmotion) VMs from an input file (CSV File). This helps me evacuate VMs from a datastore that will be decommissioned and this serves like an automation. With this, I do not have to log back to vCenter and manually do storage migrations. This also have a lesser performance impact compared to simultaneously queuing storage migrations manually in case this will be done on non-peak hours.

#Note that the CSV file must have a Header called Name, below is an example.

# Name
# VM1
# VM2
# VM3
Move-VM -VM 

$VMsToRelocate = Import-Csv "ListOfVMsToRelocate.csv"
$Datastore = Get-Datastore -Name "datastore_name"
foreach ($VM in $VMsToRelocate)
{
Write-Host "Relocating VM:" $VM.Name "to" $Datastore
Get-VM -Name $VM.Name | Move-VM -datastore $Datastore > $null
}
Write-Host "Completed!"

