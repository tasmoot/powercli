﻿# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
#                                                                   #
# Created by Tom Smoot (APL) on 2020-03-26                          #                          
#                                                                   #
# This Script will change VM Network Adapter Port Group             # 
# from a text file.                                                 #
#                                                                   #
# 1) Create a text file with one VM name per line (NO BLANK LINES!) #
# # # # # # # # # # # # # # # # # # # #  # # # # # # # # # # # # # # 

#Setting Old and New variables -- From OldPg to NewPG
$OldPG = "vmk_Management" 
$NewPG = "vmk_vMotion"

#Get a list of target VMs from a text file
$xvm = Get-Content -Path ./vm-portgroup-500-list.txt

ForEach ($VM in $xvm) {

    get-vm | Get-NetworkAdapter | Where-Object {$_.NetworkName -eq $OldPG} | Set-NetworkAdapter -Portgroup $NewPG -Confirm:$false   

}
