$i = 1
$VMs = Get-VM
$Length = $VMs.Length
ForEach ($vm in $VMs)
{
          Write-Progress -Activity "Processing Information" -Status "$vm ($i of $Length)" -PercentComplete (100*$i/$Length)
          # Do something with the object.......
          Write-Output $vm.name
          sleep 2
}

$Task = New-VM -Name $vm.name -OSCustomizationSpec  $OSCustomization -Template $Template -VMHost $VMHost -Datastore  $Datastore -RunAsync  -DiskStorageFormat Thin
Wait-Task -Task $Task