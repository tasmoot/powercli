# # # # # # # # # # # # # # # # # # # #  # # # # # # # # # # # # #
# Created by Tom Smoot ITSD/ISS on 2020-02-03 for APL.
#
# This Script will svMotion multiple VMs from a text file.
#  
# 1) Create a text file with one VM name per line (NO BLANK LINES!)
# 2) CAREFULLY Modify all $ variables to reflect appropriate targets.  -DRS Cluster in auto mode can be used.
# 3) For automation purposes the correct spec must be used.
# 4) Change location value!   
# 5) Test this script with one new VM in the text file first to verify desired result!
# -OSCustomizationSpec $spec
# # # # # # # # # # # # # # # # # # # #  # # # # # # # # # # # # #

#     $xvmhost = Read-Host "Enter the vMotion Target ESX Host (FQDN)"
# -name $_
# -destination $xvmhost
# Get-Datastore $existingdatastore | Get-VM | Move-VM -DiskStorageFormat Thin -Datastore $xnewdatastore –RunAsync

$xvm = Get-Content -Path "_svmotion-windows.txt"
#$xvmhost = get-vmhost -name cloud-03-esx103.dom1.jhuapl.edu
$xdatastore = Get-Datastore -name ntapcloud17_16
# $xdatastore = Get-DatastoreCluster -name Bldg03_D1

ForEach ($VM in $xvm) {

    Write-Host "Relocating VM:" $VM "to" $xDatastore
    Move-VM -VM $VM -Datastore $xdatastore -DiskStorageFormat Thin -confirm:$false
    Write-Host "$VM Moved Successfully" 
    Write-Host "Finished Relocating:" $VM "to" $xDatastore -ForegroundColor Green
    Write-Host "Pausing for 15 seconds to clear caches"
    Start-Sleep 120 
}
