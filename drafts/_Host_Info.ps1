<#
DeviceName         Property   string DeviceName {get;}
DhcpEnabled        Property   bool DhcpEnabled {get;}
ExtensionData      Property   System.Object ExtensionData {get;}
FullDuplex         Property   bool FullDuplex {get;}
Id                 Property   string Id {get;}
IP                 Property   string IP {get;}
Mac                Property   string Mac {get;}
Name               Property   string Name {get;}
PciId              Property   string PciId {get;}
SubnetMask         Property   string SubnetMask {get;}
Uid                Property   string Uid {get;}
VMHost             Property   VMware.VimAutomation.ViCore.Types.V1.Inventory.VMHost VMHost {get;}#>

Get-VMhost cloud-03-esx119* | Get-VMHostNetworkAdapter | Select-Object VMHost,Name,Mac,IP,Mtu,PortGroupName | Format-Table -AutoSize
$xvmhostnet = Get-VMhost cloud-03-esx107.dom1.jhuapl.edu | Get-VMHostNetworkAdapter 



Get-VMHost cloud-03-esx107.dom1.jhuapl.edu | Get-VMHostNetwork | Select-Object VMHost,Name,PhysicalNic,VirtualSwitch | Format-Table -AutoSize

Get-Cluster -Name B03Cluster100 | Get-VMHost | Get-VMHostNetwork | Select-Object Hostname, VMkernelGateway -ExpandProperty VirtualNic | Select-Object Hostname, PortGroupName, IP, SubnetMask,VMkernelGateway,Devicename | Format-Table -AutoSize

#Get Mgmt vmk
Get-Cluster -Name B03Cluster100 | Get-VMHost | Get-VMHostNetwork | Select-Object Hostname,VMkernelGateway -ExpandProperty VirtualNic | Where-Object {$_.PortGroupName -match "vmk_Management"} | Sort-Object Hostname | Select-Object Hostname,Devicename,Mac,PortGroupName,IP,SubnetMask,VMkernelGateway,Mtu | Format-Table -AutoSize

#Get vMotion vmk
Get-Cluster -Name B03Cluster100 | Get-VMHost | Get-VMHostNetwork | Select-Object Hostname, VMkernelGateway -ExpandProperty VirtualNic | Where-Object {$_.PortGroupName -match "vmk_vMotion"} | Sort-Object Hostname | Select-Object Hostname,Devicename,Mac,PortGroupName,IP,SubnetMask,VMkernelGateway,Mtu | Format-Table -AutoSize

#Get NFS vmk
Get-Cluster -Name B03Cluster100 | Get-VMHost | Get-VMHostNetwork | Select-Object Hostname, VMkernelGateway -ExpandProperty VirtualNic | Where-Object {$_.PortGroupName -match "vmk_NFS"} | Sort-Object Hostname | Select-Object Hostname,Devicename,Mac,PortGroupName,IP,SubnetMask,VMkernelGateway,Mtu | Format-Table -AutoSize


$xhostnet = Get-VMhost cloud-03-esx107.dom1.jhuapl.edu | Get-VMHostNetworkAdapter

$xhostswitch = Get-VMhost cloud-03-esx107.dom1.jhuapl.edu | Get-VDSwitch

#Cluster200 Managment
Get-Cluster -Name Cluster200 | Get-VMHost | Get-VMHostNetwork | Select-Object Hostname,VMkernelGateway -ExpandProperty VirtualNic | Where-Object {$_.PortGroupName -match "Management Network"} | Sort-Object Hostname | Select-Object Hostname,Devicename,Mac,PortGroupName,IP,SubnetMask,VMkernelGateway,Mtu | Format-Table -AutoSize

#Cluster200 vMotion
Get-Cluster -Name Cluster200 | Get-VMHost | Get-VMHostNetwork | Select-Object Hostname,VMkernelGateway -ExpandProperty VirtualNic | Where-Object {$_.PortGroupName -match "dvs-Cloud_dvPG_vmk_vMotion"} | Sort-Object Hostname | Select-Object Hostname,Devicename,Mac,PortGroupName,IP,SubnetMask,VMkernelGateway,Mtu | Format-Table -AutoSize


#B17Cluster01 Managment
Get-Cluster -Name B17Cluster01 | Get-VMHost | Get-VMHostNetwork | Select-Object Hostname,VMkernelGateway -ExpandProperty VirtualNic | Where-Object {$_.PortGroupName -match "Management Network"} | Sort-Object Hostname | Select-Object Hostname,Devicename,Mac,PortGroupName,IP,SubnetMask,VMkernelGateway,Mtu | Format-Table -AutoSize

#B17Cluster01 vMotion
Get-Cluster -Name B17Cluster01 | Get-VMHost | Get-VMHostNetwork | Select-Object Hostname,VMkernelGateway -ExpandProperty VirtualNic | Where-Object {$_.PortGroupName -match "VMotion"} | Sort-Object Hostname | Select-Object Hostname,Devicename,Mac,PortGroupName,IP,SubnetMask,VMkernelGateway,Mtu | Format-Table -AutoSize


#CloudWorkstations2 Managment
Get-Cluster -Name CloudWorkstations2 | Get-VMHost | Get-VMHostNetwork | Select-Object Hostname,VMkernelGateway -ExpandProperty VirtualNic | Where-Object {$_.PortGroupName -match "Management Network"} | Sort-Object Hostname | Select-Object Hostname,Devicename,Mac,PortGroupName,IP,SubnetMask,VMkernelGateway,Mtu | Format-Table -AutoSize

#CloudWorkstations2 vMotion
Get-Cluster -Name CloudWorkstations2 | Get-VMHost | Get-VMHostNetwork | Select-Object Hostname,VMkernelGateway -ExpandProperty VirtualNic | Where-Object {$_.PortGroupName -match "VMotion"} | Sort-Object Hostname | Select-Object Hostname,Devicename,Mac,PortGroupName,IP,SubnetMask,VMkernelGateway,Mtu | Format-Table -AutoSize


Get-cluster B03Cluster100 | Get-VMHostImageProfile