$vmnames = Import-csv "C:\vcp-vm.csv"
$vmnames | %{
    $vm = Get-VM $_.Name
    $ds = Get-Datastore -VM $vm
    $vm | Select Name, @{N="Cluster";E={Get-Cluster -VM $vm.name}},
        @{N="ESX Host";E={(Get-VMHost -VM $vm.name).Name}},
        @{N="Datastore";E={$ds.Name}},
        @{N="DS Capacity";E={$ds.CapacityMB}},
        @{N="DS Free";E={$ds.FreeSpaceMB}}
} | Export-Csv -NoTypeInformation C:\VM_CLuster_Host_Datastore.csv 

#The script is nearly the same except that it takes the $ds variable as input for the Select

$vmnames = Import-csv "C:\vcp-vm.csv" $vmnames | %{
    $vm = Get-VM $_.Name
    $ds = Get-Datastore -VM $vm    
    $ds | Select @{N="Name";E={$vm.Name}},
        @{N="Cluster";E={Get-Cluster -VM $vm.name}},
        @{N="ESX Host";E={(Get-VMHost -VM $vm.name).Name}},
        @{N="Datastore";E={$_.Name}},
        @{N="DS Capacity";E={$_.CapacityMB}},
        @{N="DS Free";E={$_.FreeSpaceMB}}
} | Export-Csv -NoTypeInformation C:\VM_CLuster_Host_Datastore.csv 