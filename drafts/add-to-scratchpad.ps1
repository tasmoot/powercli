﻿#DISCONNECT
Disconnect-VIServer $global:DefaultVIServer -Confirm:$false

#Save Credentials
#Prompt
$creds = get-credential
#Dump to encrypted file
$creds | Export-Clixml tas.cred
$creds = Import-Clixml .\tas.cred

#does this work?
Connect-VIServer "vcenter.local" -User smoott-adm -Password (Read-Host "PW?") -SaveCredentials

Connect-VIServer Server -Credential $creds

#  ** Do NOT edit top lines 2 and 3 **
& /code/_!Stop.ps1
Start-Sleep 1000
exit

$xvm = get-vm test*

Get-vm test-vm-defaults | Select Name, @{n='IPAddress'; e={$_.ExtensionData.Guest.IpAddress}}


$EsxHosts = Get-View -ViewType HostSystem -Filter @{'name'='esx1.local'}

$Vms = Get-View -ViewType VirtualMachine -Filter @{'name'='vm01'}


