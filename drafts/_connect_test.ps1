$xcomputers=@(Get-Content C:\code\powercli-main\_connect_test_list.txt)            
foreach ($computername in $xcomputers) {             
  Trap {            
        write-warning "Error Trapped for $computername"            
        write-warning $_.Exception.Message            
        Continue            
    }            
   if (Test-Connection $computername -erroraction stop  ) {             
     write-host "$computername UP" -foregroundColor Green            
   }             
   else {            
     write-host "$computername DOWN" -ForegroundColor RED            
   }            
 } 

 #Test-Connection -TargetName $xvmhost -IPv4 -Quiet
