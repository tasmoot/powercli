<#
ActiveUplinkPort                 Property   string[] ActiveUplinkPort {get;}
EnableFailback                   Property   bool EnableFailback {get;}
Failback                         Property   bool Failback {get;}
FailbackInherited                Property   bool FailbackInherited {get;}
FailoverDetectionPolicy          Property   VMware.VimAutomation.ViCore.Types.V1.Host.Networking.NetworkFailoverDetectionPolicy FailoverDetectionPolicy {get;}
FailoverDetectionPolicyInherited Property   bool FailoverDetectionPolicyInherited {get;}
LoadBalancingPolicy              Property   VMware.VimAutomation.ViCore.Types.V1.Host.Networking.LoadBalancingPolicy LoadBalancingPolicy {get;}
LoadBalancingPolicyInherited     Property   bool LoadBalancingPolicyInherited {get;}
NotifySwitches                   Property   bool NotifySwitches {get;}
NotifySwitchesInherited          Property   bool NotifySwitchesInherited {get;}
StandbyUplinkPort                Property   string[] StandbyUplinkPort {get;}
Uid                              Property   string Uid {get;}
UnusedUplinkPort                 Property   string[] UnusedUplinkPort {get;}
UplinkPortOrderInherited         Property   bool UplinkPortOrderInherited {get;}
VDPortgroup                      Property   VMware.VimAutomation.Vds.Types.V1.VDPortgroup VDPortgroup {get;}
#>
Get-VDSwitch -Name dvs-Cloud2 | Get-VDPortgroup | Get-VDUplinkTeamingPolicy | Sort-Object VDPortgroup |Format-Table VDPortgroup,ActiveUplinkPort,StandbyUplinkPort


Get-VDSwitch -Name dvs-Cloud2 | Get-VDPortgroup dvPG_1614-INET | get-vm | Get-Snapshot | Select-Object VM,Name,Description,Created,SizeGB | Format-table -AutoSize