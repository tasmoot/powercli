&{Get-VM -Server aplvcenter.dom1.jhuapl.edu | ?{$_.PowerState -eq "PoweredOn"} | %{
    $strVMName = $_.Name; Get-NetworkAdapter -VM $_ | 
        select @{n="VMName"; e={$strVMName}},Name,NetworkName,ConnectionState} | 
        ?{$_.ConnectionState.Connected -eq $false}} | 
Export-Csv -LiteralPath ~/exports/2020-10-31_disconnected_vms_aplcsv -NoTypeInformation -UseCulture 