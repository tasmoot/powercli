﻿$vcenter1 = 'aplvcenterd1.dom1.jhuapl.edu'
$vcenter2 = 'vcac-vc-dev3.dom1.jhuapl.edu'
$name1 = 'smoot*'
$name2 = 'vesx**'
 
$jobcmd = {
    $data = $input.getEnumerator()
    Import-Module VMware.VimAutomation.Core
    $connection = Connect-VIServer $data[0]
    Get-VM | Where{$_.Name -like $data[1]}
    Disconnect-VIServer $connection -Confirm:$false -Force:$true
}
 
$job1 = Start-Job -InputObject @($vcenter1,$name1) -ScriptBlock $jobcmd
$job2 = Start-Job -InputObject @($vcenter2,$name2) -ScriptBlock $jobcmd
 
while($job1.State -ne 'Completed' -or $job2.State -ne 'Completed'){
  sleep 5
  $job1 = Get-Job -Id $job1.Id
  $job2 = Get-Job -Id $job2.Id
}
 
Receive-Job -Job $job1
Receive-Job -Job $job2