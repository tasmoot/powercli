Get-VIEvent -MaxSamples 100000 -Start (Get-Date).AddDays(-2) -Type Warning | Where-Object {$_.FullFormattedMessage -match "restarted"} |Select-Object CreatedTime,FullFormattedMessage | Format-Table -AutoSize

$Date = Get-Date
$HAVMrestartold = 2
Get-VIEvent -maxsamples 100000 -Start ($Date).AddDays(-$HAVMrestartold) -type warning | Where-Object {$_.FullFormattedMessage -match "restarted"} |Select-Object CreatedTime,FullFormattedMessage |sort-object CreatedTime -Descending