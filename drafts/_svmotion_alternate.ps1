<#
Example:  /_svmotion.ps1 /vmlist.csv 4

*4 is the max running storage vMotion sessions, could be other value.
vmname,targetds
vm1,datastore3
vm20,datastore51
#>

$csvinput = $args[0]
$maxsession = $args[1]
$runsession = 0

Write-Output "---------------Summary----------------"
Write-Output "This script is to perform the Virtual Storage vMotion with give max sessions"
Write-Output "--------------------------------------"
import-csv $csvinput | foreach {

Write-Output "------------------------------------------------"
Write-Output "Checking the running vMotion now.... Pls wait...."
Write-Output "------------------------------------------------"

Do {

$runsession = (get-task | where {$_.name -like "RelocateVM_Task" -and $_.State -eq "Running" }).count

if ($runsession -ge $maxsession) {
  Write-Output "The current running vMotion sessions is $runsession.No new vMotion will be started.Next check will be performed in 1 minute."
  Start-Sleep -s 60
  get-task | where {$_.State -eq "running"}
  }

else {
  Write-Output "The current running vMotion sessions is $runsession, a new storage vMotion will be started soon."
  Start-Sleep -s 5
  }

} While ( $runsession -ge $maxsession)




Write-Output ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"
Write-Output "The Storage vMotion for will start for below VM ..."
Write-Output $_.vmname
Write-Output $_.targetds
Get-VM $_.vmname | Move-VM -Datastore $_.targetds -RunAsync -Confirm:$false
Write-Output ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"

}