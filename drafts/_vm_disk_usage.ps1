﻿
$xvm = Get-Content -Path "C:\scripts\vm-list.txt"

get-vm $xvm | Get-HardDisk | Select Parent,Name,Filename,CapacityGB,DiskType | Format-Table -AutoSize
