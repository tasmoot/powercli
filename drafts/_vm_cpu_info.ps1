$myCol = @()
foreach ($cluster in Get-Cluster) {
   foreach ($vmhost in ($cluster | Get-VMHost)) {
   $vms = Get-VM -Location $vmhost|
   where {$_.ExtensionData.Config.Hardware.NumCpu -gt $_.vmhost.ExtensionData.Hardware.CpuInfo.NumCpuPackages}
   foreach ($vm in $vms) {
   $VMView = $vm | Get-View
   $VMSummary = "" | Select ClusterName, HostName, VMName, VMSockets, VMCores, CPUSockets, CPUCores
   $VMSummary.ClusterName = $cluster.Name
   $VMSummary.HostName = $vmhost.Name
   $VMSummary.VMName = $vm.Name
   $VMSummary.VMSockets = $VMView.Config.Hardware.NumCpu
   $VMSummary.VMCores = $VMView.Config.Hardware.NumCoresPerSocket
   $VMSummary.CPUSockets = $vmhost.ExtensionData.Hardware.CpuInfo.NumCpuPackages
   $VMSummary.CPUCores = $vmhost.ExtensionData.Hardware.CpuInfo.NumCpuCores
   $myCol += $VMSummary
   }
   }
}
$myCol