$Report = @()
$VMs = Get-VM
foreach ($vm in $VMs)
{
$row = "" | Select-Object Name,OSFullName,CreationDate
$row.Name = $vm.name
$row.OSFullName = $vm.Guest.OSFullName
#$row.IP = $vm.Guest.IPAddress[0]
$row.CreationDate = $vm.ExtensionData.Config.CreateDate 
$Report += $row
}
$Report | Export-Csv -LiteralPath C:\code\export\VM_CreationDate.csv