#lucd
# https://communities.vmware.com/thread/614398
#The cluster avg CPU & mem usage can be done as follows.
#I trust you can incorporate that in your script

 

$clusterName = 'cluster'
$sStat = @{

   Entity = Get-Cluster -Name $clusterName

   Stat = 'cpu.usage.average', 'mem.usage.average'

   Start = (Get-Date).AddDays(-7)

   # Start = (Get-Date).AddHours(-1)

   Instance = '*'

   ErrorAction = 'SilentlyContinue'

}


$stat = Get-Stat @sStat


New-Object -TypeName PSObject -Property @{

   Cluster = $clusterName

   ClusterAvgCpu = (($stat | where { $_.MetricId -eq 'cpu.usage.average' }).Value | Measure-Object -Average).Average

   ClusterAvgMem = (($stat | where { $_.MetricId -eq 'mem.usage.average' }).Value | Measure-Object -Average).Average

}

 

To skip the host with the most pCPU, you could do

 

'pCPU' = ($esx.NumCpu | Sort-Object -Descending | Select -Skip 1 | Measure-Object -Sum).Sum