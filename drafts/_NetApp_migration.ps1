
Connect-VIServer -Server vcd-vc-2.dom1.jhuapl.edu

#DISCONNECT
Disconnect-VIServer $global:DefaultVIServer -Confirm:$false

#Get Cluster100 VM disk paths.   Extract list of VMs on VNX arrays for migration
 Get-Cluster B03Cluster100 | Get-VM | Get-HardDisk | Select Parent,ProvisionedSpaceGB,UsedSpaceGB,Guest,Filename | Export-Csv -Path ~/exports/cluster100_vm_disk_location-2020-05-08.csv


#Get all datastores in cloud 
Get-DatastoreCluster | Get-Datastore | Select Name,CapacityGB,FreeSpaceGB | Sort-Object -Property "FreeSpaceGB" | Export-Csv -NoTypeInformation -Path ~/exports/datastores-0507-2000.csv