﻿# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
#                                                                   #
# Created by Tom Smoot (APL) on 2020-01-21                          #                          
#                                                                   #
# This Script will create Incease VMDK size from a text file.       #
#                                                                   #
# 1) Create a text file with one VM name per line (NO BLANK LINES!) #
# # # # # # # # # # # # # # # # # # # #  # # # # # # # # # # # # # # 
$xvm = Get-Content -Path "C:\scripts\vm-list.txt"

ForEach ($VM in $xvm) {

    $HD = Get-HardDisk -VM $VM -Name "Hard disk 1"
    $NewHD = [decimal]::round($HD.CapacityGB + 30)
    Get-HardDisk -VM $VM -Name "Hard disk 1" | Set-HardDisk -CapacityGB $NewHD Confirm:$false
    
    }


