Get-VM | Get-NetworkAdapter | Select @{N="VM";E={$_.Parent.Name}},Name,Type,WakeOnLan | fl
This will display the requested properties in a list format (Format-List).
Or you could diminish the width of the columns
Get-VM | Get-NetworkAdapter | Select @{N="VM";E={$_.Parent.Name}},Name,Type,WakeOnLan | ft -Autosize
And you could also try
Get-VM | Get-NetworkAdapter | Select @{N="VM";E={$_.Parent.Name}},Name,Type,WakeOnLan | Out-Default

$myvm | select name,numcpu,memorygb,folder, @{n="IP Address"; e={$_.Guest.IpAddress}}, @{n="Mac Address"; e={$_.Guest.MacAddress}} | Format-Table -AutoSize

Get-VM -Name APLvRLTest-0*
$report = foreach($vm in Get-VM -Name APLvRLTest-0*){
    foreach($ip in $vm.Guest.IPAddress){
        $obj = [ordered]@{
            Name = $vm.Name
            Host = $vm.VMHost.Name
            IP = $ip
        }
        New-Object PSObject -Property $obj
    }
}

$report | Sort-Object -Property {($_ | Get-Member -MemberType Properties).Count} -Descending
Export-Csv 

C:\Users\gemela\Desktop\machine_ip.csv -NoTypeInformation -UseCulture


Get-VM -Name APLvRLTest-0*| Select Name,VMHost, @{N="IP Address";E={@($_.guest.IPAddress -join '|')}}