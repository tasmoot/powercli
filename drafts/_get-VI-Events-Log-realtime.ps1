#requires -pssnapin VMware.VimAutomation.Core -version 4.1
function Invoke-VIEventMonitor{
<#
.SYNOPSIS
Displays vSphere events
.DESCRIPTION
The function fetches all events and displays some properties of
each event on screen
.NOTES
Authors:	Luc Dekens
.PARAMETER Entity
Specify the vSphere object for which you want to see events. The function
will also show all events for the children of the entity.
If this parameter is not used, the function will display the events for
all the vSphere objects
.PARAMETER Finish
The time when the monitoring has to stop. The default is 2 minutes from
the start of the monitor.
.PARAMETER Pause
The time between two successive reads from the collector. The default is
2 seconds
.EXAMPLE
PS> Invoke-VIEventMonitor
.EXAMPLE
PS> Invoke-VIEventMonitor -Finish (Get-Date).AddMinutes(5) -Pause 5
.EXAMPLE
PS> Invoke-VIEventMonitor -Entity (Get-VMHost esx41*)
#>
param(
[VMware.VimAutomation.ViCore.Impl.V1.Inventory.InventoryItemImpl]$Entity = $null,
[DateTime]$Finish = (Get-Date).AddMinutes(2),
[int]$pause = 2
)
process{
Set-Variable -Name ViewSize -Value 100 -Option ReadOnly
$si = Get-View ServiceInstance
$eventMgr = Get-View $si.Content.EventManager
$filter = New-Object VMware.Vim.EventFilterSpec
if($Entity){
$filter.entity = New-Object VMware.Vim.EventFilterSpecByEntity
$filter.entity.entity = $Entity.Extensiondata.MoRef
$filter.entity.recursion = "all"
}
$filter.disableFullMessage = $false
$filter.time = New-Object VMware.Vim.EventFilterSpecByTime
$filter.time.beginTime = Get-Date
$filter.time.endTime = $Finish
$eCollector = Get-View ($eventMgr.CreateCollectorForEvents($filter))
while((Get-Date) -lt $filter.time.endTime){
$eCollector.ReadNextEvents($ViewSize) | %{
Write-Host $_.CreatedTime $_.GetType().Name $_.FullFormattedMessage
}
sleep $Pause
}
$eCollector.DestroyCollector()
}
}