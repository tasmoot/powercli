#Import the Image Builder module
Import-Module VMware.ImageBuilder
Add-EsxSoftwareDepot -DeportURL "C:\vmfs\esx-6.7.0.U3-Dell.zip"
$pkgs = Get-EsxSoftwarePackage -CreatedAfter 7/1/2010
$ip2 = New-EsxImageProfile -NewProfile -Name "Test #2" -vendor "Vendor42" -SoftwarePackage $pkgs[0]
$ip2.VibList | format-list


Get-Command -PSSnapin VMware.ImageBuilder

Add-EsxSoftwareDepot depoturl
$pkgs = Get-EsxSoftwarePackage -CreatedAfter 7/1/2010
$ip2 = New-EsxImageProfile -NewProfile -Name "Test #2" -vendor "Vendor42" -SoftwarePackage $pkgs[0]
$ip2.VibList | format-list

December 2019 Dell ESXi Image
/vmfs/esximg/VMware-VMvisor-Installer-6.7.0.update03-14320388.x86_64-DellEMC_Customized-A02.zip

shortened to:

/vmfs/esximg/esx-6.7.0.U3-Dell.zip


#Add the Depot
Add-EsxSoftwareDepot -DepotUrl .\esx-6.7.0.U3-Dell.zip

#Get the Available Profiles (list in table to get full path
Get-EsxImageProfile | format-table -AutoSize

#Clone the Dell Profile and create APL version
New-EsxImageProfile -CloneProfile DellEMC-ESXi-6.7U3-14320388-A02 -Name "APL 6.7.0.U3 DellEMC NetApp v1.0" -Vendor "APL"

#List the Partner Software Packages 
Get-EsxSoftwarePackage


#Get the NetApp VAII NFS package to load it into the Depot
Get-EsxSoftwarePackage -PackageUrl .\NetAppNasPlugin.v23.vib

#Add the NetApp package to the APL image profile
Add-EsxSoftwarePackage -ImageProfile "APL 6.7.0.U3 DellEMC NetApp v1.0" -SoftwarePackage NetAppNasPlugin

#Add the NetApp package ZIP depot
Add-EsxSoftwareDepot -DepotUrl .\NetAppNasPlugin.v23.zip


#get all the Packages
 Get-EsxSoftwarePackage | sort -Property Vendor | format-table -AutoSize


#Export the Image to .ISO
Export-EsxImageProfile -ImageProfile "APL 6.7.0.U3 DellEMC NetApp v1.0" -ExportToIso -FilePath C:\vmfs\esx_apl_6.7u3_v1.0.iso.

#Export the image to a bundle
Export-EsxImageProfile -ImageProfile "APL 6.7.0.U3 DellEMC NetApp v1.0" -ExportToBundle -FilePath C:\vmfs\esx_apl_6.7u3_v1.0.zip


#Disconnect from all software depots
Remove-EsxSoftwareDepot $DefaultSoftwareDepots



Add-PSSnapin VMware.ImageBuilder
#Get-Command -PSSnapin VMware.ImageBuilder
#Get-Help Add-EsxSoftwareDepot -Full
Add-EsxSoftwareDepot C:\CustomISOs\SoftwareDepot\update-from-esxi6.0-6.0_update02.zip
#Get-EsxImageProfile | ft -AutoSize
New-EsxImageProfile -CloneProfile ESXi-6.0.0-20160302001-standard -Name "ESXI6U2-HomeLab" -Vendor VirtualizeStuff
Set-EsxImageProfile -ImageProfile ESXI6U2-HomeLab -AcceptanceLevel CommunitySupported
#Get-EsxImageProfile -Name ESXI6U2-HomeLab | gm
#Get-EsxImageProfile -Name ESXI6U2-HomeLab | Select-Object -ExpandProperty Viblist | Sort-Object
Remove-EsxSoftwarePackage -ImageProfile ESXI6U2-HomeLab -SoftwarePackage net-e1000e
Remove-EsxSoftwarePackage -ImageProfile ESXI6U2-HomeLab -SoftwarePackage net-mlx4-en
Remove-EsxSoftwarePackage -ImageProfile ESXI6U2-HomeLab -SoftwarePackage net-mlx4-core
Remove-EsxSoftwarePackage -ImageProfile ESXI6U2-HomeLab -SoftwarePackage nmlx4-core
Remove-EsxSoftwarePackage -ImageProfile ESXI6U2-HomeLab -SoftwarePackage nmlx4-en
Remove-EsxSoftwarePackage -ImageProfile ESXI6U2-HomeLab -SoftwarePackage nmlx4-rdma
Get-EsxSoftwarePackage -PackageUrl C:\CustomISOs\VIBs\net-e1000e-2.3.2.x86_64.vib
Get-EsxSoftwarePackage -PackageUrl C:\CustomISOs\VIBs\net-e1001e-1.0.0.x86_64.vib
#Get-EsxSoftwarePackage | Sort -Property Vendor
Add-EsxSoftwarePackage -ImageProfile ESXI6U2-HomeLab -SoftwarePackage net-e1001e
Add-EsxSoftwarePackage -ImageProfile ESXI6U2-HomeLab -SoftwarePackage "net-e1000e 2.3.2"
#Get-EsxImageProfile -Name ESXI6U2-HomeLab | Select-Object -ExpandProperty Viblist | Sort-Object
Add-EsxSoftwareDepot -DepotUrl C:\CustomISOs\VIBs\mlx4_en-mlnx-1.6.1.2-offline_bundle-471530.zip
Add-EsxSoftwareDepot -DepotUrl C:\CustomISOs\VIBs\MLNX-OFED-ESX-1.8.2.4-10EM-500.0.0.472560.zip
Add-EsxSoftwareDepot -DepotUrl C:\CustomISOs\VIBs\SYN-ESX-5.5.0-NasVAAIPlugin-1.0-offline_bundle-2092790.zip
Get-EsxSoftwarePackage -SoftwareDepot C:\CustomISOs\VIBs\mlx4_en-mlnx-1.6.1.2-offline_bundle-471530.zip
Get-EsxSoftwarePackage -SoftwareDepot C:\CustomISOs\VIBs\MLNX-OFED-ESX-1.8.2.4-10EM-500.0.0.472560.zip
Get-EsxSoftwarePackage -SoftwareDepot C:\CustomISOs\VIBs\SYN-ESX-5.5.0-NasVAAIPlugin-1.0-offline_bundle-2092790.zip
Add-EsxSoftwarePackage -ImageProfile ESXI6U2-HomeLab -SoftwarePackage "net-mlx4-en 1.6.1.2-1OEM.500.0.0.406165", "net-mlx4-core 1.8.2.4-1OEM.500.0.0.472560", net-ib-core, net-ib-mad, net-ib-sa, net-ib-cm, net-mlx4-ib, net-ib-umad, scsi-ib-srp, net-ib-ipoib
Add-EsxSoftwarePackage -ImageProfile ESXI6U2-HomeLab -SoftwarePackage "esx-nfsplugin"
#Get-EsxImageProfile -Name ESXI6U2-HomeLab | Select-Object -ExpandProperty VibList | Where-Object {$_.Vendor -notlike "VMware"} | Sort-Object
Export-EsxImageProfile -ImageProfile ESXI6U2-HomeLab -ExportToIso -FilePath C:\CustomISOs\ESXI6U2-HomeLab.iso
Export-EsxImageProfile -ImageProfile ESXI6U2-HomeLab -ExportToBundle -FilePath C:\CustomISOs\ESXI6U2-HomeLab.zipAdd-PSSnapin VMware.ImageBuilder
#Get-Command -PSSnapin VMware.ImageBuilder
#Get-Help Add-EsxSoftwareDepot -Full
Add-EsxSoftwareDepot C:\CustomISOs\SoftwareDepot\update-from-esxi6.0-6.0_update02.zip
#Get-EsxImageProfile | ft -AutoSize
New-EsxImageProfile -CloneProfile ESXi-6.0.0-20160302001-standard -Name "ESXI6U2-HomeLab" -Vendor VirtualizeStuff
Set-EsxImageProfile -ImageProfile ESXI6U2-HomeLab -AcceptanceLevel CommunitySupported
#Get-EsxImageProfile -Name ESXI6U2-HomeLab | gm
#Get-EsxImageProfile -Name ESXI6U2-HomeLab | Select-Object -ExpandProperty Viblist | Sort-Object
Remove-EsxSoftwarePackage -ImageProfile ESXI6U2-HomeLab -SoftwarePackage net-e1000e
Remove-EsxSoftwarePackage -ImageProfile ESXI6U2-HomeLab -SoftwarePackage net-mlx4-en
Remove-EsxSoftwarePackage -ImageProfile ESXI6U2-HomeLab -SoftwarePackage net-mlx4-core
Remove-EsxSoftwarePackage -ImageProfile ESXI6U2-HomeLab -SoftwarePackage nmlx4-core
Remove-EsxSoftwarePackage -ImageProfile ESXI6U2-HomeLab -SoftwarePackage nmlx4-en
Remove-EsxSoftwarePackage -ImageProfile ESXI6U2-HomeLab -SoftwarePackage nmlx4-rdma
Get-EsxSoftwarePackage -PackageUrl C:\CustomISOs\VIBs\net-e1000e-2.3.2.x86_64.vib
Get-EsxSoftwarePackage -PackageUrl C:\CustomISOs\VIBs\net-e1001e-1.0.0.x86_64.vib
#Get-EsxSoftwarePackage | Sort -Property Vendor
Add-EsxSoftwarePackage -ImageProfile ESXI6U2-HomeLab -SoftwarePackage net-e1001e
Add-EsxSoftwarePackage -ImageProfile ESXI6U2-HomeLab -SoftwarePackage "net-e1000e 2.3.2"
#Get-EsxImageProfile -Name ESXI6U2-HomeLab | Select-Object -ExpandProperty Viblist | Sort-Object
Add-EsxSoftwareDepot -DepotUrl C:\CustomISOs\VIBs\mlx4_en-mlnx-1.6.1.2-offline_bundle-471530.zip
Add-EsxSoftwareDepot -DepotUrl C:\CustomISOs\VIBs\MLNX-OFED-ESX-1.8.2.4-10EM-500.0.0.472560.zip
Add-EsxSoftwareDepot -DepotUrl C:\CustomISOs\VIBs\SYN-ESX-5.5.0-NasVAAIPlugin-1.0-offline_bundle-2092790.zip
Get-EsxSoftwarePackage -SoftwareDepot C:\CustomISOs\VIBs\mlx4_en-mlnx-1.6.1.2-offline_bundle-471530.zip
Get-EsxSoftwarePackage -SoftwareDepot C:\CustomISOs\VIBs\MLNX-OFED-ESX-1.8.2.4-10EM-500.0.0.472560.zip
Get-EsxSoftwarePackage -SoftwareDepot C:\CustomISOs\VIBs\SYN-ESX-5.5.0-NasVAAIPlugin-1.0-offline_bundle-2092790.zip
Add-EsxSoftwarePackage -ImageProfile ESXI6U2-HomeLab -SoftwarePackage "net-mlx4-en 1.6.1.2-1OEM.500.0.0.406165", "net-mlx4-core 1.8.2.4-1OEM.500.0.0.472560", net-ib-core, net-ib-mad, net-ib-sa, net-ib-cm, net-mlx4-ib, net-ib-umad, scsi-ib-srp, net-ib-ipoib
Add-EsxSoftwarePackage -ImageProfile ESXI6U2-HomeLab -SoftwarePackage "esx-nfsplugin"
#Get-EsxImageProfile -Name ESXI6U2-HomeLab | Select-Object -ExpandProperty VibList | Where-Object {$_.Vendor -notlike "VMware"} | Sort-Object
Export-EsxImageProfile -ImageProfile ESXI6U2-HomeLab -ExportToIso -FilePath C:\CustomISOs\ESXI6U2-HomeLab.iso
Export-EsxImageProfile -ImageProfile ESXI6U2-HomeLab -ExportToBundle -FilePath C:\CustomISOs\ESXI6U2-HomeLab.zip