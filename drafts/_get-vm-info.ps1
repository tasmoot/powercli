$VmInfo = ForEach ($Datacenter in (Get-Datacenter)) {
    ForEach ($VM in ($Datacenter | Get-VM | sort |  where {$_.powerstate -match "on"} | Get-VMGuest)) {
    $vm | Select-Object @{N="VM_NAME#";E={$vm.Hostname}} ,
        @{N="VM_CPU_Core#";E={$VM.VM.NumCPU}},
        @{N="VM_MemoryGB";E={$VM.VM.MemoryGB}},
        @{N="VM_DiskGB";E={[math]::Round(($VM.VM.Extensiondata.Guest.Disk |
            Measure-Object -Property Capacity -Sum | Select-Object -ExpandProperty Sum)/1GB,0)}},
        @{N="VM_IP#";E={$vm.IPAddress}},
        @{N="VM_OS";E={$vm.OSFullName}},
        @{N="VM_DC";E={$Datacenter.name}},
        @{N='VM_Cluster';E={(Get-Cluster -VM $VM.VM).Name}},
        @{N="VM_NOTES";E={$VM.VM.Notes}},
        @{N='VM_Annotations';E={(Get-Annotation -Entity $VM.VM).Value}}
    }
}

#$VmInfo | Export-Csv -Path "c:\scripts\vm_info_2020-01-21.csv" -NoTypeInformation -UseCulture