Get-VIRole

$role = get-virole
$role.extensiondata | 
Select-Object -Property Name,@{Name='Privilege';expression={[string]::Join(";",($_.Privilege))}} |
Export-Csv -path C:\Temp\get-viRole.csv -NoTypeInformation 


Get-Folder ITC | Get-VIPermission | Select-Object Role,Principal,Entity,Propagate


#Get-Inventory -Location (Get-Datacenter "Dev View") | Get-Permissions | Export-Csv -Path "~/exports/permissions.csv" -NoTypeInformation