function FailSafe {
    
        1..10000 | ForEach { 
            Write-Host ""
            Write-Host ""
            Write-Host "**************************************************************"
            Write-Host "*******  WTF!?   --- Do NOT press F5 in this window!!  *******"
            Write-Host "**************************************************************"
            Write-Host ""
            Write-Host ""
            Write-Host ""
            Start-Sleep -seconds 10
            Write-Host ""
            Write-Host ""
            Write-Host ""
            }   
}