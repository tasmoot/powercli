$esx = Get-VMHost -Name cloud-03-esx301.dom1.jhuapl.edu
$netSys = Get-View -Id $esx.ExtensionData.ConfigManager.NetworkSystem

$netSys.NetworkInfo.Vswitch |

ForEach-Object -Process {

    $vss = $_

    $vss.Pnic |

    Select @{N='Switch';E={$vss.name}},

        @{N='Type';E={'VSS'}},

        @{N='pNIC';E={$_.Split('-')[-1]}}

}

$netSys.NetworkInfo.ProxySwitch |

ForEach-Object -Process {

    $vds = $_

    $vds.Pnic |

    Select @{N='Switch';E={$vds.DvsName}},

        @{N='Type';E={'VDS'}},

        @{N='pNIC';E={$_.Split('-')[-1]}}

}