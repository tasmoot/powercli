﻿
<# old
$datastore = Get-Datastore -name data1
$template = get-template -name fsccv2_ws08r2std_v1.2
$vmhost = get-vmhost -name host.corp.com
$spec = Get-OSCustomizationSpec -name Auto #>


$xvm = Get-Content -Path "C:\scripts\vm-list.txt"


ForEach-Object
    ($VM in (Get-VM | Get-View)){($VM.Guest.Disk) |
        Select `
        @{N=“Name“;E={$VM.Name}},DiskPath, @{N=“Capacity(GB)“; E={[math]::Round($_.Capacity/ 1GB)}}, `
        @{N=“Free Space(GB)“;E={[math]::Round($_.FreeSpace / 1GB)}}, `
        @{N=“Free Space %“;E={[math]::Round(((100 * ($_.FreeSpace))/ ($_.Capacity)),0)}}) | Format-Table -auto}
    
    }
}
        

    #orginal code
ForEach ($VM in (Get-VM -Name $xvm | Get-View)){($VM.Guest.Disk |
Select `
@{N=“Name“;E={$VM.Name}},DiskPath, @{N=“Capacity(GB)“; E={[math]::Round($_.Capacity/ 1GB)}}, `
@{N=“Free Space(GB)“;E={[math]::Round($_.FreeSpace / 1GB)}}, `
@{N=“Free Space %“;E={[math]::Round(((100 * ($_.FreeSpace))/ ($_.Capacity)),0)}}) | Format-Table -auto}

   