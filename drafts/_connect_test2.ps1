$xcomputers=@(Get-Content ./_connect_test_list.txt)            

ForEach-Object ($computername in $xcomputers) { 
  $xPingTest = Test-Connection $computername -IPv4 -Quiet -Count 2
             if ($xPingTest -eq $true)
              {             
     write-host "$computername UP" -foregroundColor Green            
   } 
            else {             
     write-host "$computername DOWN" -ForegroundColor RED            
   }                       
             
 } 