$xCluster = "L278_B17VNX5600"
$outputFile = "~/exports/TEST_vm_standby.csv" 

$VMsAdv = Get-Datastore -Name $xCluster | Get-VM | Sort-Object Name | % { Get-View $_.ID } 
$myCol = @() 
ForEach ($VMAdv in $VMsAdv) 
{ 
    ForEach ($Disk in $VMAdv.Layout.Disk) 
    { 
        $myObj = "" | Select-Object Name, Disk 
        $myObj.Name = $VMAdv.Name 
        $myObj.Disk = $Disk.DiskFile[0] 
        $myCol += $myObj 
    } 
} 
$myCol | Export-Csv $outputFile -NoTypeInformation