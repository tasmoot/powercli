# # # # # # # # # # # # # # # # # # # #  # # # # # # # # # # # # #
# Created by Tom Smoot ITSD/ISS on 2020-02-03 for APL.
#
# This Script will create multiple VMs from a text file.
#  
# 1) Create a text file with one VM name per line (NO BLANK LINES!)
# 2) CAREFULLY Modify all $ variables to reflect appropriate targets.  -DRS Cluster in auto mode can be used.
# 3) For automation purposes the correct spec must be used.
# 4) Change location value!   
# 5) Test this script with one new VM in the text file first to verify desired result!
# -OSCustomizationSpec $spec
# # # # # # # # # # # # # # # # # # # #  # # # # # # # # # # # # #

$xdatastore = Get-Datastore -name ntapcloud17_01
$xtemplate = get-template -name ntap-w2012
$xvmhost = get-vmhost -name cloud-03-esx101.dom1.jhuapl.edu
$xspec = Get-OSCustomizationSpec -name Windows-NoDomain 

Get-Content "vm-list.txt" |

ForEach-Object {

    New-VM -Name $_ -Template $xtemplate -VMHost $xvmhost -datastore $xdatastore -description "NTAP Test VMs" -location "ntap-test1" -confirm:$false
    }
  
    

