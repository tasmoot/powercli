$report = @()
$eventMgr = Get-View EventManager
$events = foreach($event in $eventMgr.Description.EventInfo){
New-Object PSOBject -Property @{
Key = $event.Key
Category = $event.Category
EventTypeId = &{
If('ExtendedEvent','EventEx' -contains $event.Key){
$event.FullFormat.Split('|')[0]
}
else{ $null}}
Description = $event.Description
}
}
$events | Export-Csv -Path ~/exports/events.csv -NoTypeInformation -UseCulture