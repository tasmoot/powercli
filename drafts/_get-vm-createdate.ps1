
$VMinfo = foreach($vm in (Get-VM)){
    $vm | Select-Object 
    @{N="Creation Date";E={$vm.ExtensionData.Config.CreateDate}}

$VMinfo | Select-Object Name,

@{N="VM_NAME#";E={$vm.Hostname}} ,
        @{N="VM_CPU_Core#";E={$VM.VM.NumCPU}},
        @{N="VM_MemoryGB";E={$VM.VM.MemoryGB}},
        @{N="VM_DiskGB";E={[math]::Round(($VM.VM.Extensiondata.Guest.Disk |
            Measure-Object -Property Capacity -Sum | Select-Object -ExpandProperty Sum)/1GB,0)}},
        @{N="VM_IP#";E={$vm.IPAddress}},
        @{N="VM_OS";E={$vm.OSFullName}},
        @{N="VM_DC";E={$Datacenter.name}},
        @{N='VM_Cluster';E={(Get-Cluster -VM $VM.VM).Name}},
        @{N="VM_NOTES";E={$VM.VM.Notes}},
        @{N='VM_Annotations';E={(Get-Annotation -Entity $VM.VM).Value}}
        @{N="VM_CreationDate";E={$VM.ExtensionData.Config.createDate}}
    }

$VmInfo | Export-Csv -Path "c:\code\export\<MACHINE_NAME>_vm_info_2022-10-08.csv" -NoTypeInformation -UseCulture