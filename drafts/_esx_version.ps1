Get-View -ViewType HostSystem -Property Name, Config.Product,Parent |
select Name,
    @{N='Product';E={$_.Config.Product.FullName}},
    @{N='Build';E={$_.Config.Product.Build}},
    @{N='Cluster';E={
      $parent = Get-View -Id $_.Parent -Property Name,Parent
      While ($parent -isnot [VMware.Vim.ClusterComputeResource] -and $parent.Parent){
        $parent = Get-View -Id $parent.Parent -Property Name,Parent
      }
      if($parent -is [VMware.Vim.ClusterComputeResource]){
        $parent.Name}}} |
Format-Table -auto