Get-Datastore |

Select-Object Name,@{N="vCenter";E={$_.Uid.Split('@')[1].Split(':')[0]}},`
@{N="Datacenter";E={$_.Datacenter}},`
CapacityGB,@{N='UsedGB';E={[math]::Round($_.CapacityGB - $_.FreeSpaceGB)}},`
FreeSpaceGB, @{N="VMs";E={$_.ExtensionData.Vm.Count}} |

    #good
    #@{N='Used';E={($_.CapacityGB - $_.FreeSpaceGB)}},
    #@{N='Used';E={[math]::Round($_.CapacityGB - $_.FreeSpaceGB)}},
       #/$_.CapacityGB)}}
    #@{N='In Use %';E={'{0:p1}' -f (($_.CapacityGB - $_.FreeSpaceGB + $_.ExtensionData.Summary.Uncommitted/1GB)/$_.CapacityGB)}} |
    #@{N='Provisioned';E={'{0:N1}' -f ($_.CapacityGB - $_.FreeSpaceGB + $_.ExtensionData.Summary.Uncommitted/1GB)}},
    #@{N='Real Free';E={($_.ExtensionData.Summary.Uncommitted/1GB)}},
    #{[math]::Round($_.FreeSpace / 1GB)}
    #@{N='Free %';E={'{0:p1}' -f ($_.CapacityGB/$_.FreeSpaceGB)}},
    #@{N='ProvisionedGB';E={'{0:N1}' -f [math]::Round(($_.CapacityGB - $_.FreeSpaceGB))}},
    #@{N='Provisioned %';E={'{0:p1}' -f (($_.CapacityGB - $_.FreeSpaceGB)/$_.CapacityGB)}},
    #@{N='In Use GB';E={'{0:N1}' -f ($_.CapacityGB - $_.FreeSpaceGB + $_.ExtensionData.Summary.Uncommitted/1GB)}},

 Out-GridView
 # Export-Csv -Path c:\code\export\Datastore_Report_-$((Get-Date).ToString('yyyy-MM-dd-hh-mm')).csv -NoTypeInformation   
