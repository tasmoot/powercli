$report = @()
foreach ($dc in Get-Datacenter) {

    foreach ($cluster in Get-Cluster -Location $dc){
      $datastores = Get-view -ViewType Datastore -SearchRoot $cluster.Name
     # $datastores = Get-view -ViewType Datastore -Filter @{'name'='VxRail-Virtual-SAN-Datastore-b23b2efc-6eac-4dca-9534-3092e9d04f9c'}

      foreach ($datastore in $datastores){

        $info = "" | Select-Object Name

        #$info.IPAddress = ($vm.Guest.net.IPAddress | where{$_} | Sort-Object -Unique) -join '|'

        $info.Datastores = (Get-View -Id $datastores.Name -Property Name).Name -join '|'

        $info.datacenter = $dc.name

        $info.Name = $datastore.name

        $report += $info

      }

    }

}

$report | Out-GridView
# | export-csv C:\code\export\Datastore_Report.csv -NoTypeInformation