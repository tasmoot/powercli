$report = @()
foreach ($vc in $global:DefaultVIServers) {

	    ForEach-Object -Process {

	    	        Get-Datastore -Server $_ -PipelineVariable ds |

	    	                ForEach-Object -Process {

	    	                	            New-PSDrive -Location $ds -Name DS -PSProvider VimDatastore -Root '\' | Out-Null

	    	                	                        $report += Get-ChildItem -Path DS:\ISO -Filter *.iso -Recurse |

	    	                	                                    Select-Object @{N = 'vCenter'; E = { $vc.Name } },

	    	                	                                                     @{N = 'Datastore'; E = { $ds.Name } },

	    	                	                                                                      Name, FolderPath

	    	                	                                                                                  Remove-PSDrive -Name DS -Confirm:$false

	    	                	                                                                                          }

	    	                	                                                                                              }

	    	                	                                                                                              }


	    	                	                                                                                              $report | Export-Csv -Path C:\code\export\datastore_file_report.csv -NoTypeInformation -UseCulture
	    	                }
	    }
}
