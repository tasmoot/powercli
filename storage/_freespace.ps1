$xdatastorecluster = Read-Host "Enter the Name of the Datastore Cluster"

Get-DatastoreCluster $xdatastorecluster | Get-Datastore | Sort-Object FreeSpaceGB

#Get Datastores with < 1 TB (1,000 GB) of free space with name of L*
Get-Datastore L*| Where-Object {$_.FreeSpaceGB -le 1000} | Sort-Object Name

Get-Datastore L*| Where-Object {$_.FreeSpaceGB -le 1000} | Sort-Object FreeSpaceGB

Get-Datastore Clu L*| Where-Object {$_.FreeSpaceGB -le 1000} | Sort-Object FreeSpaceGB

Get-Datastore | gm

#How to write this with conditions?   Want all greater than 3TB capacity, but with less than 1TB free.
Get-Datastore | Where-Object {($_.CapacityGB -gt 3000 -and $_.FreeSpaceGB -le 1000)} 


Get-DatastoreCluster B03VNX5400_Cloud | get-datastore | select Name,CapacityGB,FreeSpaceGB, @{N='VM Count';E={$_.ExtensionData.VM.Count}} | Sort-Object FreeSpaceGB -Descending

$dscluster = 'B17VNX5600_Cloud'
Get-DatastoreCluster $dscluster | get-vm | Select-Object Name,UsedSpaceGB | Export-Csv -NoTypeInformation -Path ~/exports/5600_2021-03-24-$dscluster.csv

$dscluster = 'B17VNX5600_Cloud2'
Get-DatastoreCluster $dscluster | Get-Template | Get-NetworkAdapter | Select-Object Parent,NetworkName |Format-Table -AutoSize
