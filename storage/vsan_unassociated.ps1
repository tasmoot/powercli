$clustername = "VxRail-Virtual-SAN-Cluster-b23b2efc-6eac-4dca-9534-3092e9d04f9c"
$clusterView = Get-Cluster $ClusterName
$ClusterMoRef = $Clusterview.ExtensionData.MoRef
$vmhost = ($clusterView | Get-VMHost) | select -First 1
$vsanIntSys = Get-View $vmhost.ExtensionData.configManager.vsanInternalSystem

$vsanClusterObjectSys = Get-VsanView -Id VsanObjectSystem-vsan-cluster-object-system
$results = (($vsanClusterObjectSys.VsanQueryObjectIdentities($clusterMoRef,$null,$null,$true,$true,$false)).Identities | where {$_.Vm -eq $null})

foreach ($result in $results) {
$jsonResult = ($vsanIntSys.GetVsanObjExtAttrs($result.Uuid)) | ConvertFrom-JSON
foreach ($object in $jsonResult | Get-Member) {
if($($object.Name) -ne "Equals" -and $($object.Name) -ne "GetHashCode" -and $($object.Name) -ne "GetType" -and $($object.Name) -ne "ToString") {
$objectID = $object.name
$jsonResult.$($objectID) 
}
}
} 
