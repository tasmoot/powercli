$xdscluster = "ENTER DS CLUSTER NAME"

$VMs = Get-DatastoreCluster -Name $xdscluster | Get-VM
$report = @()
Foreach ($VM in $VMs){
    $line = $VM | Select-Object Name, @{N="Memory (GB)";E={($_).MemoryGB}}, @{N="Datastore";E={$_.ExtensionData.Config.Files.VmPathName}}
    $report += $line
    }
    $report | Export-csv -NoTypeInformation -Path ~/exports/Suspend-Datastores_with_VMs-2.csv -Append


