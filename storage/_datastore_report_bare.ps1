Get-Datastore |
Select-Object Name,

@{N='Datacenter';E='$_.Datacenter.Name'}, @{N='Used Space(%)';E={[math]::Round((($_.CapacityGB - $_.FreeSpaceGB)/$_.CapacityGB*100),1)}}