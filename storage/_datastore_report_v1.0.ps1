Get-Datastore |

Select-Object Name,@{N="vCenter";E={$_.Uid.Split('@')[1].Split(':')[0]}},`
@{N="Datacenter";E={$_.Datacenter}},`
CapacityGB,@{N='UsedGBRounded';E={[math]::Round($_.CapacityGB)}},@{N='UsedGB';E={[math]::Round($_.CapacityGB - $_.FreeSpaceGB)}},`
FreeSpaceGB, @{N="VMs";E={$_.ExtensionData.Vm.Count}} |


# Out-GridView
 Export-Csv -Path c:\code\export\Datastore_Report_-$((Get-Date).ToString('yyyy-MM-dd-hh-mm')).csv -NoTypeInformation   
