$EsxHosts = Get-VMhost #-Name <MACHINE_NAME>-dcon-vxr01.lab.net

#foreach ($cluster in $Clusters) { 
    foreach ($esxhost in $Esxhosts) {
        Write-Host "Stopping SSH on $esxhost"  
    Get-VMHostService -VMHost $esxhost | Where { $_.Key -eq "TSM-SSH" } | Stop-VMHostService -Confirm:$false 
}


