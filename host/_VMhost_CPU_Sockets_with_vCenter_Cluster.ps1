﻿#Get ESX CPU Sockets
## get the Spec inventory for All ESX Hosts

#clear arrays
$result = @() 

#Set time variable for unique CSV export 
$TimeStamp = Get-Date -Format “yyyy-MM-dd-hh”

## Use this section to get ALL vmhosts and clusters (Comment "#" -Filter.. out to query all Hosts)
$EsxHosts = Get-view  -ViewType HostSystem #-Filter @{'name'='<MACHINE_NAME>-4cn-esx001.lab.net'}

#foreach ($cluster in $Clusters) { 
    foreach ($esxhost in $Esxhosts) {
     $obj = new-object psobject
     $obj | Add-Member -MemberType NoteProperty -Name vCenter -Value $global:DefaultVIServer 
     $obj | Add-Member -MemberType NoteProperty -Name Cluster -Value (Get-View -Id $ESXHost.Parent -Property Name).Name
     $obj | Add-Member -MemberType NoteProperty -Name EsxHostName -Value $EsxHost.Name
     #$obj | Add-Member -MemberType NoteProperty -Name IPAddess -Value (Get-VMHost $ESXHost.Name | Select-Object @{n="ManagementIP"; e={Get-VMHostNetworkAdapter -VMHost $_ -VMKernel | ?{$_.ManagementTrafficEnabled} | %{$_.Ip}}})
     $obj | Add-Member -MemberType NoteProperty -Name IPAddess -Value (Get-VMHostNetworkAdapter -VMHost $ESXHost.Name -VMKernel | ?{$_.ManagementTrafficEnabled} | %{$_.Ip})
     $obj | Add-Member -MemberType NoteProperty -Name vSphereVersion -Value $EsxHost.Summary.Config.Product.Version
     $obj | Add-Member -MemberType NoteProperty -Name vSphereBuild -Value $EsxHost.Summary.Config.Product.Build
     $obj | Add-Member -MemberType NoteProperty -Name Vendor -Value $EsxHost.Hardware.SystemInfo.Vendor
     $obj | Add-Member -MemberType NoteProperty -Name Model -Value $EsxHost.Hardware.SystemInfo.Model
     $obj | Add-Member -MemberType NoteProperty -Name ServiceTag -Value ($EsxHost.Hardware.SystemInfo.OtherIdentifyingInfo | Where-Object {$_.IdentifierType.Key -eq "ServiceTag"}).IdentifierValue     
     #$obj | Add-Member -MemberType NoteProperty -Name BootTime -Value $EsxHost.Runtime.BootTime   
     $obj | Add-Member -MemberType NoteProperty -Name CpuGeneration -Value $EsxHost.Summary.MaxEVCModeKey  
     #$obj | Add-Member -MemberType NoteProperty -Name EvcMode -Value $EsxHost.Summary.CurrentEVCModeKey  
     $obj | Add-Member -MemberType NoteProperty -Name CpuModel -Value $EsxHost.Summary.Hardware.CpuModel   
     $obj | Add-Member -MemberType NoteProperty -Name NoCpuSockets -Value $EsxHost.hardware.CpuInfo.NumCpuPackages
     $obj | Add-Member -MemberType NoteProperty -Name CpuCoresPerSocket -Value ($EsxHost.hardware.CpuInfo.NumCpuCores / $EsxHost.Summary.Hardware.NumCpuPkgs)  
     $obj | Add-Member -MemberType NoteProperty -Name TotalCpuCores -Value $EsxHost.hardware.CpuInfo.NumCpuCores 
     $obj | Add-Member -MemberType NoteProperty -Name GhzPerSocket -Value ($EsxHost.Summary.Hardware.CpuMhz /1000)
     $obj | Add-Member -MemberType NoteProperty -Name GhzCapacity -Value ([math]::Truncate((($EsxHost.Summary.Hardware.CpuMhz /1000  * ($EsxHost.hardware.CpuInfo.NumCpuCores)))))
     #$obj | Add-Member -MemberType NoteProperty -Name HzTotal -Value ([math]::Truncate((($EsxHost.hardware.CpuInfo.Hz * ($EsxHost.hardware.CpuInfo.NumCpuCores)))))
     $obj | Add-Member -MemberType NoteProperty -Name MemoryTotalGb -Value ([math]::Round((($EsxHost.Hardware.MemorySize)/(1GB)),1))
     $result += $obj
    }
#}
 ## provide full details
 #$result | Format-Table -AutoSize *
  $result | Sort-Object Cluster | Format-Table -AutoSize *

## count the ESX Hosts
 Write-Output "Total ESX hosts:"
  $result.Count

#Write-Output "Overview of all sockets and cores per ESX host:"

 ## calculate the total sockets
  Write-Output "Total Socket count from all ESX hosts:"
 ($result | Measure-Object -Property NoCpuSockets -Sum).Sum
 
 ## calculate the total cores
  Write-Output "Total Core Count over all Sockets from all ESX hosts:"
 ($result | Measure-Object -Property TotalCpuCores -Sum).sum

## Out Grid View
$result | Out-GridView

 ## export to CSV 
$result | Sort-Object EsxHostName | Export-Csv -NoTypeInformation -Append -LiteralPath C:\code\export\ESX_host_specs-$TimeStamp.csv
 

## ===========================================================
## End script
## ===========================================================

