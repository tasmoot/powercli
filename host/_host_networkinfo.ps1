## get the Network Info for ALl ESX Hosts

#Set time variable for unique CSV export 


Get-VMHost | Get-VMHostNetworkAdapter | Select-Object VMHost,Name,IP,SubnetMask,Mac,PortGroupName | 
Export-Csv -LiteralPath C:\code\export\ESX_host_network-" + "_" + (Get-Date).tostring("yyyy-MM-dd-hh-mm") + ".csv


