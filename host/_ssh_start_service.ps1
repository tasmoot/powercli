# This script will start the SSH service on specified hosts.  If the SSH service is already running, no changes are made.
# Hosts can be specified just using the Get-VMhost command with no parameters which targets all ESX hosts in the vCenter
# by using the Get-VMhost with "-Name" to specify specific hosts 

$EsxHosts = Get-VMhost #-Name <MACHINE_NAME>-dcon-vxr01.lab.net

#foreach ($cluster in $Clusters) { 
    foreach ($esxhost in $Esxhosts) {
Write-Host "Starting SSH on $esxhost"
Get-VMHostService -VMHost $esxhost | Where { $_.Key -eq "TSM-SSH" } | Start-VMHostService -Confirm:$false
    }