#Use esxcli to extract serial number

foreach($esxcli in Get-VMHost -Name itsd-03-esx104.dom1.jhuapl.edu | Get-EsxCli -V2){
    $esxcli.hardware.platform.get.Invoke() | Select-Object SerialNumber}
    #Select-Object @{N='VMHost';E={$esxcli.VMHost.Name}},SerialNumber}#,VendorName,ProductName,
    #Select-Object SerialNumber,VendorName,ProductName


#One Liner:
#Get-VMHost -Name itsd-03-esx104.dom1.jhuapl.edu | Select-Object Name, @{N='Serial';E={(Get-EsxCli -V2 -VMHost $_).hardware.platform.get().SerialNumber}} 
#Get-VMHost -Name itsd-03-esx104.dom1.jhuapl.edu | Get-EsxCli -V2) {$esxcli.hardware.platform.get.Invoke() | Select-Object SerialNumber} 
#foreach($esxcli in Get-VMhost $xvmhost | Get-EsxCli -v2) {$esxcli.hardware.platform.get.Invoke() | Select-Object SerialNumber} 
Get-EsxCli -V2 -Server $global:DefaultVIServer -VMHost itsd-03-esx104.dom1.jhuapl.edu | foreach-object {$_.hardware.platform.get.Invoke() | Select-Object SerialNumber}