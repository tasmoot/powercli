Get-Cluster | %{
    $cluster = $_
    $esx = Get-VMHost -Location $cluster
    $numcpu = $esx | %{$_.ExtensionData.Hardware.CpuInfo.NumCpuPackages} | Measure-Object -Sum |
      Select -ExpandProperty Sum
    $numcores = $esx | %{$_.ExtensionData.Hardware.CpuInfo.NumCpuCores} | Measure-Object -Sum |
      Select -ExpandProperty Sum
    Get-VM -Location $cluster |  Select Name,
      @{N="Host";E={$_.Host.Name}},
      @{N="Cluster";E={$cluster.Name}},
      @{N="Total Hosts/Cluster";E={$esx.Count}},
      @{N="Total CPU/Cluster";E={$numcpu}},
      @{N="Total Cores/Cluster";E={$numcores}}
  }