$result = @() 

$ESXCli = Get-VMhost

Foreach ($VMHost in Get-VMHost ) {

    ForEach-Object -Process {
        $esxcli = Get-EsxCli -VMHost $_ -V2
        $esxcli.system.module.list.Invoke() | ForEach-Object {
            $ESXCli.system.module.get($_.Name) | Select-Object `
             @{N="VMHost";E={$VMHost}},Module,License,Modulefile,Version,SignedStatus,SignatureDigest,SignatureFingerPrint |
             Format-Table -AutoSize
    }
 
 }
} 

