﻿#Get-View for speed

#Quick for Small Environments
Get-VMHost | Select Name,PowerState

#Faster for large environments
Get-View -ViewType HostSystem | select Name, @{N="PowerState"; E={$_.Runtime.PowerState

# Simple VM report -> Use NoType for clean CSV
Get-VM | Select Name,NumCpu,MemoryGB | Export-Csv "U:\vm-report3.csv" -NoTypeInformation -UseCulture

#MacAddress Function
$report = @()

Get-View -ViewType VirtualMachine -Filter @{"summary.runtime.powerstate"="poweredon"} | Select -Property Name,
Get-View -ViewType VirtualMachine -Filter @{"summary.runtime.powerstate"="poweredoff"} | Select -Property Name

Get-View -ViewType VirtualMachine | Get-Member

Accepted types:
ClusterComputeResource
ComputeResource
Datacenter
Datastore
DistributedVirtualPortgroup
DistributedVirtualSwitch
Folder
HostSystem 
Network
OpaqueNetwork
ResourcePool
StoragePod
VirtualApp
VirtualMachine
VmwareDistributedVirtualSwitc







