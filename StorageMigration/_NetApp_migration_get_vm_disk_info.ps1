
Get-Cluster Cluster200 | Get-VM | Select-Object Name,UsedSpaceGB, `

@{n='Operating System'; e={$_.ExtensionData.Summary.Guest.GuestFullName}}, `
#@{n='Hostname'; e={$_.ExtensionData.Summary.Guest.HostName}}, `
#@{n='IP Address'; e={$_.ExtensionData.Summary.Guest.IpAddress}}, `
#@{n='MacAddress'; e={$_.ExtensionData.Guest.Net.MacAddress}}, `
#@{n='NumCPU'; e={$_.ExtensionData.summary.config.NumCpu}}, `
#@{n='MemorySizeMB'; e={$_.ExtensionData.summary.config.MemorySizeMB}}, `
#@{n='NIC Cards'; e={$_.ExtensionData.summary.config.NumEthernetCards}}, `
@{n='Virtual Disks'; e={$_.ExtensionData.summary.config.NumVirtualDisks}}, `
#@{n='Template'; e={$_.ExtensionData.summary.config.Template}}, `
#@{n='ToolsStatus'; e={$_.ExtensionData.Summary.Guest.ToolsStatus}}, `
@{n='Datastore Path'; e={$_.ExtensionData.Summary.Config.VmPathName}} `
| Export-Csv -NoTypeInformation -Path ~/exports/NetApp_VM_2021-03-17-Cluster200.csv