Get-DatastoreCluster B03VNX5400_Cloud | get-datastore | select Name,CapacityGB,FreeSpaceGB, @{N='VM Count';E={$_.ExtensionData.VM.Count}} | Sort-Object FreeSpaceGB -Descending

$dscluster = 'B17VNX5600_Cloud'
Get-DatastoreCluster $dscluster | get-vm | Select-Object Name,UsedSpaceGB | Export-Csv -NoTypeInformation -Path ~/exports/5600_2021-03-24-$dscluster.csv

$dscluster = 'B17VNX5600_Cloud'
Get-DatastoreCluster $dscluster | Get-Template | Get-NetworkAdapter | Select-Object Parent,NetworkName |Format-Table -AutoSize
